常用服务端脚手架
Springboot 2.2.6
mybatis
redis
mysql数据库
maven

提供了
1、后台管理服务
        商户  角色   权限菜单  管理员  系统参数
2、短信验证码
3、文章列表
       常见问题
       平台通知
       首页轮播图
4、文件上传（base64、multipartFile、文件地址接受方式）
        上传到本地
        上传到存储服务
             阿里云oss （开发中）
             腾讯cos （存储桶创建， 文件上传）
             又拍云 （开发中）
             七牛云 （开发中）
5、用户服务
      多种登录方式（qq  微信  微博等）
6、三大支付
      微信支付 （当前只有wap）
      支付宝支付（当前只有wap）
      银联支付 开发中
7、dao mapper.xml 自动生成