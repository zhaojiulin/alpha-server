package com.alpha.common.utils;

public class Constants {
    public static final String MERCHANT_ID = "merchantId";
    public static final String UPLOAD_LOCAL_BASE_PATH = "src/main/resources/static/";
}
