package com.alpha.common.utils;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * 枚举常量
 * @author zhaojiulin
 * @DATE 2020/2/27 13:34
 */
public class Enums {

    /**
     * 统一返回常量
     */
    public enum BusinessMsgEnum {

        OK(1,"操作成功"),
        ERROR(-1,"操作异常"),
        PARAMETER_EXCEPTION(102,"参数异常"),
        MISS_PARAMETER_EXCEPTION(400,"缺少必要的请求参数"),
        UNEXPECTED_EXCEPTION(500,"系统发生异常，请联系管理员");

        private int code;
        private String name;
        private BusinessMsgEnum(int code, String name){
            this.code = code;
            this.name = name;
        }

        private static final Map<Integer, BusinessMsgEnum> RESULT_CODE_MAP = new HashMap<>();
        static {
            for (BusinessMsgEnum rc : EnumSet.allOf(BusinessMsgEnum.class)) {
                RESULT_CODE_MAP.put(rc.getCode(), rc);
            }
        }

        public static String get(int code) {
            BusinessMsgEnum rc = RESULT_CODE_MAP.get(code);
            return rc.getName();
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     * 登录类型
     */
    public enum IdentityType {
        QQ("qq"),
        WX("wx"),
        WEIBO("weibo"),
        EMAIL("email"),
        MOBILE("mobile"),
        MOBILE_CODE("mobileCode");

        String value;
        private String describe;
        private IdentityType(String  value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    /**
     * 常见缓存key
     */
    public enum CacheKeys {
        Key("key", "系统密钥"),
        ApiKey("api_key", "对外api密钥"),
        ESign("e_sign",""),
        SmsWay("sms_way", "短信渠道"),
        BaseUrl("base_url", "系统基础域名"),
        StaticPath("static_url", "系统"),
        ApiBaseUrl("api_base_url", "对外接口基础域名");

        private String value;
        private String describe;
        private CacheKeys(String value, String describe) {
            this.value = value;
            this.describe = describe;
        }

        public String getValue() {
            return this.value;
        }

        public String getDescribe() {
            return describe;
        }

        public void setDescribe(String describe) {
            this.describe = describe;
        }
    }

    public enum UploadFileType {
        Base64Str(1),
        OriginPath(2),
        Multipart(3);

        private Integer code;

        private UploadFileType(Integer code) {
            this.code = code;
        }

        public Integer getCode() {
            return this.code;
        }
    }

    public enum PayWays {
        AliPay(1,"AliPay"),
        UnionPay(2,"UnionPay"),
        WxPay(3,"WxPay");

        int code;
        String value;
        private PayWays(int code, String value) {
            this.code = code;
            this.value = value;
        }

        private final static Map<Integer,PayWays> PAY_WAYS_MAP = new HashMap<>();
        static {
            for (PayWays payWays: EnumSet.allOf(PayWays.class)) {
                PAY_WAYS_MAP.put(payWays.getCode(),payWays);
            }
        }

        public static String get(int code) {
            PayWays pw = PAY_WAYS_MAP.get(code);
            return pw.getValue();
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
