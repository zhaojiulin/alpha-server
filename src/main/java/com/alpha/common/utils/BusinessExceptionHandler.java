package com.alpha.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常拦截
 * @author zhaojiulin
 * @date 2020/2/27 13:34
 */
@RestControllerAdvice
public class BusinessExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(BusinessExceptionHandler.class);

   /* @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public Res handleHttpMessageNotReadableException(MissingServletRequestParameterException ex){
        logger.error("缺少请求参数,{}",ex.getMessage());
        return Res.msg(BusinessMsgEnum.MISS_PARAMETER_EXCEPTION.getCode(),BusinessMsgEnum.MISS_PARAMETER_EXCEPTION.getName());
    }*/

    /**
     * 空指针异常
     * @param ex
     * @return
     */
    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Res HandleNullException(NullPointerException ex){
        logger.error("空指针异常，{}",ex.getMessage());
        return Res.fail("空指针异常");
    }

    @ExceptionHandler(BusinessException.class)
    public Res HandleBusinessException(BusinessException ex){
        return Res.fail(ex.getMessage());
    }
}
