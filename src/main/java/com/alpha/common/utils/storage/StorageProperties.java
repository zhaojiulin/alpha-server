package com.alpha.common.utils.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

/**
 * 文件存储配置类
 */
@Component
@ConfigurationProperties(value = "storage")
public class StorageProperties {
    /** 阿里云oss 存储配置 */
    private AliyunOssSet aliyunSet;
    /** 腾讯云cos 存储配置 */
    private TencentCosSet tencentCosSet;
    /** 七牛云 存储配置 */
    private QiniuSet qiniuSet;
    /** 本地存储配置 */
    private LocalSet localSet;

    public AliyunOssSet getAliyunSet() {
        return aliyunSet;
    }

    public void setAliyunSet(AliyunOssSet aliyunSet) {
        this.aliyunSet = aliyunSet;
    }

    public TencentCosSet getTencentCosSet() {
        return tencentCosSet;
    }

    public void setTencentCosSet(TencentCosSet tencentCosSet) {
        this.tencentCosSet = tencentCosSet;
    }

    public QiniuSet getQiniuSet() {
        return qiniuSet;
    }

    public void setQiniuSet(QiniuSet qiniuSet) {
        this.qiniuSet = qiniuSet;
    }

    public LocalSet getLocalSet() {
        return localSet;
    }

    public void setLocalSet(LocalSet localSet) {
        this.localSet = localSet;
    }

    public static class AliyunOssSet {
        private String endpoint;
        private String accessKeyId;
        private String accessKeySecret;
        private String bucketName;

        public String getEndpoint() {
            return endpoint;
        }

        public void setEndpoint(String endpoint) {
            this.endpoint = endpoint;
        }

        public String getAccessKeyId() {
            return accessKeyId;
        }

        public void setAccessKeyId(String accessKeyId) {
            this.accessKeyId = accessKeyId;
        }

        public String getAccessKeySecret() {
            return accessKeySecret;
        }

        public void setAccessKeySecret(String accessKeySecret) {
            this.accessKeySecret = accessKeySecret;
        }

        public String getBucketName() {
            return bucketName;
        }

        public void setBucketName(String bucketName) {
            this.bucketName = bucketName;
        }
    }

    public static class TencentCosSet {
        private String appId;
        private String secretId;
        private String secretKey;
        private String region;
        private String bucketName;

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getSecretId() {
            return secretId;
        }

        public void setSecretId(String secretId) {
            this.secretId = secretId;
        }

        public String getSecretKey() {
            return secretKey;
        }

        public void setSecretKey(String secretKey) {
            this.secretKey = secretKey;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getBucketName() {
            return bucketName;
        }

        public void setBucketName(String bucketName) {
            this.bucketName = bucketName;
        }
    }

    public static class QiniuSet {
        private String endpoint;
        private String accessKey;
        private String secretKey;
        private String bucketName;

        public String getEndpoint() {
            return endpoint;
        }

        public void setEndpoint(String endpoint) {
            this.endpoint = endpoint;
        }

        public String getAccessKey() {
            return accessKey;
        }

        public void setAccessKey(String accessKey) {
            this.accessKey = accessKey;
        }

        public String getSecretKey() {
            return secretKey;
        }

        public void setSecretKey(String secretKey) {
            this.secretKey = secretKey;
        }

        public String getBucketName() {
            return bucketName;
        }

        public void setBucketName(String bucketName) {
            this.bucketName = bucketName;
        }
    }

    public static class LocalSet {
        private String storagePath;
        private String host;
        private Path rootPath;

        public String getStoragePath() {
            return storagePath;
        }

        public void setStoragePath(String storagePath) {
            this.storagePath = storagePath;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public Path getRootPath() {
            return rootPath;
        }

        public void setRootPath(Path rootPath) {
            this.rootPath = rootPath;
        }
    }
}
