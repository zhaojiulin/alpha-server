package com.alpha.common.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 统一返回包装类
 * @author zhaojiulin
 * @date 2020/2/27 13:34
 */
public class Res {
    /** 状态 */
    private int code;
    /** 状态 */
    private boolean success;
    /** 信息 */
    private String message;
    /** 数据 */
    private Map<String,Object> result = new HashMap<>();

    public Res(){
        super();
    }

    public static Res ok(){
        return ok(Enums.BusinessMsgEnum.OK.getName());
    }

    public static Res ok(String key, Object value){
        return ok().add(key,value);
    }

    public static Res ok(String message){
        return new Res(true,message);
    }

    public static Res fail(){
        return fail(Enums.BusinessMsgEnum.ERROR.getName());
    }

    public static Res fail(String key, Object value){
        return fail().add(key,value);
    }

    public static Res fail(String message){
        return new Res(false,message);
    }

    private Res(boolean flag, String message){
        this.setRes(flag);
        this.message = message;
    }

    public static Res msg(int code, String message){
        return new Res(code,message);
    }

    private Res(int code, String message){
        this.code = code;
        this.message = message;
    }

    private void setRes(boolean success) {
        if (success) {
            this.code = 1;
        } else {
            this.code = -1;
        }
        this.success = success;
    }

    public Res add(String key, Object value) {
        this.result.put(key,value);
        return this;
    }

    public Res add(Object value) {
        this.result.put("data",value);
        return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }
}
