package com.alpha.common.utils.message;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.Enums;
import com.alpha.components.GlobalConfigMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class SendSmsService {

    private final SendSmsProperties sendSmsProperties;

    @Autowired
    public SendSmsService(SendSmsProperties sendSmsProperties) {
        this.sendSmsProperties = sendSmsProperties;
    }

    private SendSms sendSms;

    /**
     * 初始化短信通道
     */
    void initSmsWay() {
        String smsWay = GlobalConfigMap.getConfig(Enums.CacheKeys.SmsWay.getValue());
        if (Objects.equals(smsWay, "aliyun")) {
            sendSms = new AliyunSendSms(sendSmsProperties);
        }else if (Objects.equals(smsWay, "tencent")) {
            sendSms = new TencentSendSms(sendSmsProperties);
        } else {
            throw new BusinessException("不支持当前短信发送模式" + smsWay);
        }
    }

    /**
     * 发送短线
     * @param smsParam
     */
    public void send(SmsParam smsParam) {
        initSmsWay();
        sendSms.send(smsParam);
    }
}
