package com.alpha.common.utils.message;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 短信发送配置类
 */
@Component
@ConfigurationProperties(prefix = "sms")
public class SendSmsProperties {
    private TencentCloud tencentCloud;

    public TencentCloud getTencentCloud() {
        return tencentCloud;
    }

    public void setTencentCloud(TencentCloud tencentCloud) {
        this.tencentCloud = tencentCloud;
    }

    public static class TencentCloud {
        private int appId;
        private String appkey;

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public String getAppKey() {
            return appkey;
        }

        public void setAppKey(String appkey) {
            this.appkey = appkey;
        }


    }
}
