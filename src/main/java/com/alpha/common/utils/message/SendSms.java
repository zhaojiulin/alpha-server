package com.alpha.common.utils.message;

public interface SendSms {
    void send(SmsParam smsParam);
}
