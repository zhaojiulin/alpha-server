package com.alpha.common.utils.message;

import com.github.qcloudsms.SmsSingleSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class TencentSendSms implements SendSms{
    private final static Logger LOGGER = LoggerFactory.getLogger(TencentSendSms.class);

    private final SendSmsProperties smsProperties;

    private final SmsSingleSender sender;

    public TencentSendSms(SendSmsProperties properties) {
        smsProperties = properties;
        sender = new SmsSingleSender(smsProperties.getTencentCloud().getAppId(), smsProperties.getTencentCloud().getAppKey());
    }

    public void send(SmsParam smsParam) {
        LOGGER.info("发送短信：{}", Arrays.toString(smsParam.getMobile()));
        SendSmsProperties.TencentCloud tencentSmsConfig = smsProperties.getTencentCloud();

    }
}
