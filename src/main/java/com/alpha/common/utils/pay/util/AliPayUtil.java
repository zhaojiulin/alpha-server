package com.alpha.common.utils.pay.util;

import com.alipay.api.CertAlipayRequest;
import com.alipay.api.domain.AlipayOpenOperationOpenbizmockBizQueryModel;
import com.alipay.api.request.AlipayOpenOperationOpenbizmockBizQueryRequest;

import java.util.Map;

public class AliPayUtil {
    public static final String URL = "https://openapi.alipay.com/gateway.do";

    public static CertAlipayRequest getAliPayClientParams(Map<String, String> aliConfig) {
        CertAlipayRequest certParams = new CertAlipayRequest();
        certParams.setServerUrl(URL);
        //AppId
        certParams.setAppId(aliConfig.get("appId"));
        //PKCS8格式的应用私钥
        certParams.setPrivateKey("MIIEvQIBADANB ... ...");
        //使用的字符集编码，采用utf-8
        certParams.setCharset("utf-8");
        certParams.setFormat("json");
        certParams.setSignType("RSA2");
        //应用公钥证书文件路径
        certParams.setCertPath("src/cert/appCertPublicKey_2019091767145019.crt");
        //支付宝公钥证书文件路径
        certParams.setAlipayPublicCertPath("/home/foo/alipayCertPublicKey_RSA2.crt");
        //支付宝根证书文件路径
        certParams.setRootCertPath("/home/foo/alipayRootCert.crt");
        return certParams;
    }

    public static AlipayOpenOperationOpenbizmockBizQueryRequest getRequest() {
        // 初始化Request，并填充Model属性。实际调用时请替换为您想要使用的API对应的Request对象。
        AlipayOpenOperationOpenbizmockBizQueryRequest request = new AlipayOpenOperationOpenbizmockBizQueryRequest();
        AlipayOpenOperationOpenbizmockBizQueryModel model = new AlipayOpenOperationOpenbizmockBizQueryModel();
        model.setBizNo("test");
        request.setBizModel(model);
        return request;
    }
}
