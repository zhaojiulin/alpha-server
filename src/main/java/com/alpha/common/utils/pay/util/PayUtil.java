package com.alpha.common.utils.pay.util;


import com.alpha.module.sys.dao.SysConfigMapper;
import com.alpha.module.sys.entity.SysConfig;
import com.alpha.common.utils.CommonUtils;
import com.alpha.common.utils.SpringBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;

import java.util.Map;

public class PayUtil {
    private static final Logger logger = LoggerFactory.getLogger(PayUtil.class);
    public static String createOrderNo(String pre) {
        String no = "";
        no += pre;
        no += LocalDateTime.now();
        return no;
    }

    public static String currentTime(String pre) {
        String no = "";
        no += LocalDateTime.now();
        return no;
    }

    public static Map<String, String> getPayConfig(String domainName, String configKey) {
        Map<String, String> config = null;
        SysConfigMapper sysConfigMapper = SpringBeanUtil.getBean(SysConfigMapper.class);
        SysConfig sysConfig = null;
        if (sysConfigMapper != null) {
            sysConfig = sysConfigMapper.selectByDomainName(domainName);
        }
        Map<String, String> payConfig = null;
        if (sysConfig != null) {
            payConfig = CommonUtils.jsonStrToMap(sysConfig.getValue());
        }
        if (payConfig != null) {
            config = CommonUtils.jsonStrToMap(payConfig.get(configKey));
        }
        return config;
    }

    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "application/json");
            conn.setRequestProperty("Content-type", "application/json; charset=utf-8");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setConnectTimeout(1000 * 30);
            conn.setReadTimeout(1000 * 30);
            conn.setUseCaches(false);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            logger.warn("发送 POST 请求出现异常！{}", e.getMessage());
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

}
