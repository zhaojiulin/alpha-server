package com.alpha.common.utils.pay.util;

import com.alibaba.fastjson.JSONObject;
import com.alpha.module.app.model.PayDTO;
import com.alpha.components.RedisService;
import com.alpha.common.utils.SecurityUtil;
import com.alpha.common.utils.StrUtils;
import com.alpha.common.utils.SpringBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.*;

public class WxPayUtil {
    private static final Logger logger  = LoggerFactory.getLogger(WxPayUtil.class);

    // 微信支付 - 统一下单地址
    public static final String URL_PAY_H5 = "https://api.mch.weixin.qq.com/pay/unifiedorder";



    public static HashMap<String, String> createWxParam(Map<String, String> wxConfig, PayDTO payDTO) {
        HashMap<String, String> packageParams = new HashMap<>();
        packageParams.put("appid", wxConfig.get("appId"));
        packageParams.put("mch_id", wxConfig.get("mchId"));
        packageParams.put("body", payDTO.getBody());
        packageParams.put("nonce_str", StrUtils.generateNonceStr());
        packageParams.put("notify_url", wxConfig.get("notifyUrl"));
        packageParams.put("trade_type", "MWEB");
        packageParams.put("out_trade_no", payDTO.getOutTradeNo());
        packageParams.put("total_fee", payDTO.getTotalAmount());
        packageParams.put("spbill_create_ip", payDTO.getIp());
        return packageParams;
    }

    public static String getAccessToken(String accessTokenKey, String appid, String secret) {
        RedisService redisService = SpringBeanUtil.getBean(RedisService.class);
        // 缓存
        String access_token = redisService.get(accessTokenKey);
        if (!ObjectUtils.isEmpty(access_token)) {
            return access_token;
        }
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + secret;
        String resultStr = PayUtil.sendPost(url, "");
        JSONObject json = JSONObject.parseObject(resultStr);
        access_token = json.getString("access_token");
        redisService.set(accessTokenKey, access_token);
        redisService.expire(accessTokenKey,60 * 60 * 60);
        return access_token;
    }

    public static String generateSignature(final Map<String, String> data, String key) {
        Set<String> keySet = data.keySet();
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        Arrays.sort(keyArray);
        StringBuilder sb = new StringBuilder();
        for (String k : keyArray) {
            if (k.equals("sign")) {
                continue;
            }
            if (data.get(k).trim().length() > 0) // 参数值为空，则不参与签名
                sb.append(k).append("=").append(data.get(k).trim()).append("&");
        }
        sb.append("key=").append(key);
        return SecurityUtil.MD5(sb.toString()).toUpperCase();
    }

    public static Map<String, String> xmlToMap(String strXML) throws Exception {
        try {
            Map<String, String> data = new HashMap<String, String>();
            DocumentBuilder documentBuilder = newDocumentBuilder();
            InputStream stream = new ByteArrayInputStream(strXML.getBytes("UTF-8"));
            Document doc = documentBuilder.parse(stream);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getDocumentElement().getChildNodes();
            for (int idx = 0; idx < nodeList.getLength(); ++idx) {
                Node node = nodeList.item(idx);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    data.put(element.getNodeName(), element.getTextContent());
                }
            }
            try {
                stream.close();
            } catch (Exception ex) {
                // do nothing
                ex.printStackTrace();
            }
            return data;
        } catch (Exception ex) {
            logger.warn("Invalid XML, can not convert to map. Error message: {}. XML content: {}", ex.getMessage(), strXML);
            throw ex;
        }

    }

    public static String mapToXml(Map<String, String> data) throws Exception {
        Document document = newDocument();
        Element root = document.createElement("xml");
        document.appendChild(root);
        for (String key : data.keySet()) {
            String value = data.get(key);
            if (value == null) {
                value = "";
            }
            value = value.trim();
            org.w3c.dom.Element filed = document.createElement(key);
            filed.appendChild(document.createTextNode(value));
            root.appendChild(filed);
        }
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        DOMSource source = new DOMSource(document);
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
        String output = writer.getBuffer().toString(); //.replaceAll("\n|\r", "");
        try {
            writer.close();
        } catch (Exception ex) {
        }
        return output;
    }

    public static DocumentBuilder newDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        documentBuilderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        documentBuilderFactory.setXIncludeAware(false);
        documentBuilderFactory.setExpandEntityReferences(false);

        return documentBuilderFactory.newDocumentBuilder();
    }

    public static Document newDocument() throws ParserConfigurationException {
        return newDocumentBuilder().newDocument();
    }
}
