package com.alpha.common.utils.pay.ways;

import com.alpha.common.utils.pay.PayInterface;
import com.alpha.module.app.model.PayDTO;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component(value = "UnionPay")
public class UnionPay implements PayInterface {

    @Override
    public void payRequest(PayDTO payDTO) {
        System.out.println("银联支付");
    }

    @Override
    public void pcPay(PayDTO payDTO) {

    }

    @Override
    public String h5Pay(PayDTO payDTO) {
        return "";
    }

    @Override
    public void appPay(PayDTO payDTO) {

    }

    @Override
    public void callBack(Map<String, String> params) {

    }
}
