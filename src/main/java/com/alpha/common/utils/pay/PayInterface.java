package com.alpha.common.utils.pay;

import com.alpha.module.app.model.PayDTO;

import java.util.Map;

public interface PayInterface {
    void payRequest(PayDTO payDTO);
    void pcPay(PayDTO payDTO);
    String h5Pay(PayDTO payDTO);
    void appPay(PayDTO payDTO);
    void callBack(Map<String, String> params);
}
