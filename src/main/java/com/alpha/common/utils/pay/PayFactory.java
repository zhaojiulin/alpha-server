package com.alpha.common.utils.pay;

import com.alpha.common.utils.SpringBeanUtil;

public class PayFactory {
    public static PayInterface create(String beanName) {
        return (PayInterface) SpringBeanUtil.applicationContext.getBean(beanName);
    }
}
