package com.alpha.common.utils.pay.util;


public class PayConfig {

    // 微信配置
    public static final String WX_CONFIG = "wxConfig";
    // 支付宝配置
    public static final String ALIPAY_CONFIG = "aliPayConfig";
    public static final String notifyUrl = "";
    public static final String returnUrl = "";
}
