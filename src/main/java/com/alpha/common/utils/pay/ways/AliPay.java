package com.alpha.common.utils.pay.ways;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.request.AlipayOpenOperationOpenbizmockBizQueryRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayOpenOperationOpenbizmockBizQueryResponse;
import com.alpha.common.utils.pay.PayInterface;
import com.alpha.common.utils.pay.util.AliPayUtil;
import com.alpha.common.utils.pay.util.PayConfig;
import com.alpha.module.app.model.PayDTO;
import com.alpha.common.utils.pay.util.PayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.alipay.api.DefaultAlipayClient;

import java.util.Map;

@Component(value = "AliPay")
public class AliPay implements PayInterface {
    private static final Logger logger = LoggerFactory.getLogger(PayInterface.class);
    public static final String URL = "https://openapi.alipay.com/gateway.do";
    public static final String APP_ID = "";
    public static final String PRIVATE_KEY = "";
    public static final String ALIPAY_PUBLIC_KEY = "";

    @Override
    public void payRequest(PayDTO payDTO) {
        System.out.println("支付宝支付");
    }

    @Override
    public void pcPay(PayDTO payDTO) {
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(URL, APP_ID, PRIVATE_KEY, "json", "utf-8", ALIPAY_PUBLIC_KEY, "RSA2");
            AlipayOpenOperationOpenbizmockBizQueryRequest request = AliPayUtil.getRequest();
            AlipayOpenOperationOpenbizmockBizQueryResponse response = alipayClient.certificateExecute(request);
            if (response.isSuccess()) {
                System.out.println("调用成功。");
            } else {
                System.out.println("调用失败，原因：" + response.getMsg() + "，" + response.getSubMsg());
            }
        } catch (Exception e) {
            System.out.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }

    }

    @Override
    public String h5Pay(PayDTO payDTO) {
        try {
            Map<String, String> aliConfig = PayUtil.getPayConfig(payDTO.getDomainName(), PayConfig.ALIPAY_CONFIG);
            AlipayClient alipayClient = new DefaultAlipayClient(AliPayUtil.getAliPayClientParams(aliConfig));
            // 创建API对应的request
            AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();
            alipayRequest.setReturnUrl(aliConfig.get("returnUrl"));
            // 在公共参数中设置回跳和通知地址
            alipayRequest.setNotifyUrl(aliConfig.get("notifyUrl"));
            alipayRequest.setBizContent("{" +
                    " \"out_trade_no\":\"" + payDTO.getOutTradeNo() + "\"," +
                    " \"total_amount\":\""+ payDTO.getTotalAmount() +"\"," +
                    " \"subject\":\""+ payDTO.getSubject() +"\"," +
                    " \"product_code\":\"QUICK_WAP_PAY\"" +
                    " }");//填充业务参数
            // 调用SDK生成表单
            String form = alipayClient.pageExecute(alipayRequest).getBody();
            return form;
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void appPay(PayDTO payDTO) {

    }

    @Override
    public void callBack(Map<String, String> params) {

    }
}
