package com.alpha.common.utils;

/**
 * @author zhaojiulin
 * @date 2020/2/27 14:42
 */
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -7034897190745766989L;

    private int code;

    private String message;
    public BusinessException(String message){
      super(message);
      this.message = message;
    }

    public BusinessException(Enums.BusinessMsgEnum businessMsgEnum){
        this.code = businessMsgEnum.getCode();
        this.message = businessMsgEnum.getName();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
