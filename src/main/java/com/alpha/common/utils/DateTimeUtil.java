package com.alpha.common.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 时间工具类
 * @author zhaojiulin
 * @DATE 2020/2/27 13:34
 */
public class DateTimeUtil {

    public static String nowDateStr1() {
        return nowStr("yyyy-MM-dd");
    }

    public static String nowDateStr2() {
        return nowStr("yyyyMMdd");
    }

    public static String nowTimeStr1() {
        return nowStr("HH:mm:ss");
    }

    public static String nowDateTimeStr1() {
        return nowStr("yyyy-MM-dd HH:mm:ss");
    }

    public static String nowDateTimeStr2() {
        return nowStr("yyyyMMddHHmmss");
    }

    public static String nowStr(String fmtStr) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(fmtStr);
        return dateTimeFormatter.format(LocalDateTime.now());
    }

    /**
     * 时区当前时间
     * @return
     */
    public static Date zoneNowDate() {
        return Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
    }
}
