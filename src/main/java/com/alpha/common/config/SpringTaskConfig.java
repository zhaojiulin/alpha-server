package com.alpha.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 开启定时任务能力
 */
@Configuration
@EnableScheduling
public class SpringTaskConfig {
}
