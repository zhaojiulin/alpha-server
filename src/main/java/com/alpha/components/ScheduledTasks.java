package com.alpha.components;

import com.alpha.module.blog.entity.Blog;
import com.alpha.module.blog.service.BlogService;
import com.alpha.module.user.service.UserService;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * ScheduledTasks
 * 定时任务类
 * @author zhaojiulin
 * @time 2020/6/30 9:19 上午
 */
@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    @Autowired
    private UserService userService;
    @Autowired
    private BlogService blogService;

    /**
     * 每分钟一次喝水提醒
     */
    @Scheduled(cron = "0 0/15 * ? * ?")
    private void drinkWaterNotice() {
        logger.info("该喝水了。。。。");
        // userService.notifyDrink();
    }
}
