package com.alpha.components.annotation;

import java.lang.annotation.*;

/**
 * 自定义注解  限流
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceLimit {

    String description() default "";

    String key() default "";

    LimitType limitType() default LimitType.CUSTOMER;

    enum LimitType {
        CUSTOMER,
        IP
    }
}
