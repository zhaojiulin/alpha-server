package com.alpha.components;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class GlobalConfigMap {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalConfigMap.class);

    protected static Map<String, String> configs = new HashMap<>();

    /**
     * 将配置添加到公共集合中
     * @param key 键
     * @param value 值
     */
    public static void addConfig(String key, String value) {
        configs.put(key, value);
    }

    public static void updateKeyConfig(String key, String value) {
        String s = configs.get(key);
        LOGGER.info("旧键值对：" + key + "--" + s);
        LOGGER.info("新键值对：" + key + "--" + value);
        configs.put(key, value);
    }

    /**
     * 获取String字符串值
     * @param key 键
     * @return
     */
    public static String getConfig(String key) {
        return configs.get(key);
    }

    /**
     * 获取Integer值
     * @param key 键
     * @return
     */
    public static Integer getConfigInt(String key) {
        return Integer.parseInt(configs.get(key));
    }

    /**
     * 获取BigDecimal值
     * @param key 键
     * @return
     */
    public static BigDecimal getConfigBigDecimal(String key) {
        return new BigDecimal(configs.get(key));
    }

    /**
     * 获取并转为JSON值
     * @param key 键
     * @return
     */
    public static JSONObject getConfigJson(String key) {
        try {
            return JSONObject.parseObject(configs.get(key));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 转为实体格式
     * @param key 键
     * @param cls 实体类字节码
     * @param <T>
     * @return
     */
    public static <T> T getConfigPojo(String key, Class<T> cls) {
        return JSONObject.parseObject(configs.get(key), cls);
    }


    /**
     * 系统是否开发状态 true 开发， false，否
     * @return
     */
    public static boolean envIsDev() {
        String config = getConfig("alpha.system.env");
        return Objects.equals(config, "dev");
    }
}
