package com.alpha.module.app.dao;

import com.alpha.module.app.entity.Article;
import com.alpha.module.app.model.ArticleDTO;


import java.util.List;

public interface ArticleMapper {

    List<Article> queryList(ArticleDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(Article record);

    int insertSelective(Article record);

    Article selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKeyWithBLOBs(Article record);

    int updateByPrimaryKey(Article record);
}