package com.alpha.module.app.service.impl;

import com.alpha.module.app.dao.ArticleMapper;
import com.alpha.module.app.entity.Article;
import com.alpha.module.app.model.ArticleDTO;
import com.alpha.module.app.service.ArticleService;
import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service("articleService")
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleMapper articleMapper;

    @Override
    public PageUtils queryPage(ArticleDTO articleDTO) {
        PageHelper.startPage(articleDTO.getPageNo(), articleDTO.getPageSize());
        List<Article> list = articleMapper.queryList(articleDTO);
        List<ArticleDTO> models = new ArrayList<>();
        for (Article article : list) {
            ArticleDTO model = ArticleDTO.instance(article);
            models.add(model);
        }
        return new PageUtils(new PageInfo(models));
    }

    @Override
    public void save(Article article) {
            articleMapper.insertSelective(article);
    }

    @Override
    public Article getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return articleMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(Article article) {
            articleMapper.updateByPrimaryKeySelective(article);
    }


}