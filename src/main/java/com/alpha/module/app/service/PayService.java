package com.alpha.module.app.service;

import com.alpha.module.app.model.PayDTO;

import java.util.Map;

public interface PayService {

    Map<String, String> payRequest(PayDTO vo);

    void pcPayRequest(PayDTO vo);

    void h5PayRequest(PayDTO vo);

    void appPayRequest(PayDTO vo);

    void callBack(Map<String, String> params, String payWay);
}
