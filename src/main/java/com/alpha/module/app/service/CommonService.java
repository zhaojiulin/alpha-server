package com.alpha.module.app.service;

import com.alpha.module.app.model.SmsCodeDTO;
import com.alpha.module.app.model.UploadFileDTO;
import org.springframework.web.multipart.MultipartFile;

public interface CommonService {

    String smsCode(SmsCodeDTO smsCodeDTO);

    void verifySmsCode(SmsCodeDTO smsCodeDTO);

    String uploadFile(UploadFileDTO file);

    String uploadMultipartFile(MultipartFile file);

    String uploadBase64File(UploadFileDTO uploadFileDTO);
}
