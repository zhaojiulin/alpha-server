package com.alpha.module.app.service.impl;


import com.alpha.common.utils.message.SendSmsService;
import com.alpha.common.utils.message.SmsParam;
import com.alpha.components.GlobalConfigMap;
import com.alpha.module.app.model.SmsCodeDTO;
import com.alpha.module.app.model.UploadFileDTO;
import com.alpha.module.app.service.CommonService;
import com.alpha.components.RedisService;
import com.alpha.common.utils.*;
import com.alpha.common.utils.storage.TencentCos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Random;

@Service("commonService")
public class CommonServiceImpl implements CommonService {
    @Autowired
    private RedisService redisService;

    @Autowired
    private SendSmsService sendSmsService;

    /**
     * 短信验证码
     * @param smsCodeDTO 短信传输
     * @return success
     */
    @Override
    public String smsCode(SmsCodeDTO smsCodeDTO) {
        StringBuilder code = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            code.append(random.nextInt(10));
        }
        // 将验证码存入redis
        redisService.set("msg_" + smsCodeDTO.getType() + smsCodeDTO.getMobile(), code.toString());
        // 设置有效期
        redisService.expire("msg_" + smsCodeDTO.getType() + smsCodeDTO.getMobile(),120);
        // 开发环境
        if (GlobalConfigMap.envIsDev()) {
            return code.toString();
        }
        // 短信发送
        SmsParam smsParam = new SmsParam();
        smsParam.setMobile(new String[] { smsCodeDTO.getMobile() });
        smsParam.setContent("验证码：" + code.toString());
        sendSmsService.send(smsParam);
        return code.toString();
    }

    @Override
    public void verifySmsCode(SmsCodeDTO smsCodeDTO) {
        if (StringUtils.isEmpty(smsCodeDTO.getCode())) {
            throw new BusinessException("请输入验证码");
        }
        String cacheCode = redisService.get("msg_" + smsCodeDTO.getType() + smsCodeDTO.getMobile());
        boolean result = smsCodeDTO.getCode().equals(cacheCode);
        if (!result) {
            throw new BusinessException("验证码不正确");
        }
    }

    @Override
    public String uploadFile(UploadFileDTO uploadFileDTO) {
        if (Objects.isNull(uploadFileDTO.getOriginPath())) {
            return "";
        }
        File file = new File(uploadFileDTO.getOriginPath());
        return TencentCos.cosUploadFile(file, "/" + uploadFileDTO.getParentPath() + "/"+ uploadFileDTO.getFileName());
    }

    @Override
    public String uploadMultipartFile(MultipartFile file) {
        String sourceUrl = "";
        try {
            String fileName = file.getOriginalFilename();
            sourceUrl = TencentCos.cosUploadFile(file.getInputStream(),fileName);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return sourceUrl;
    }

    @Override
    public String uploadBase64File(UploadFileDTO uploadFileDTO) {
        String fileType = uploadFileDTO.getBase64().split(";")[0].split("/")[1];
        InputStream is = FileUtils.base64StringToInputSteam(uploadFileDTO);
        if (uploadFileDTO.getFileName() == null) {
            uploadFileDTO.setFileName(DateTimeUtil.zoneNowDate().getTime() + "");
        }
        return TencentCos.cosUploadFile(is, uploadFileDTO.getParentPath() + "/"+ uploadFileDTO.getFileName() + "." + fileType);
  }
}
