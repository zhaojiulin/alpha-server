package com.alpha.module.app.service.impl;

import com.alpha.common.utils.pay.PayFactory;
import com.alpha.common.utils.pay.PayInterface;
import com.alpha.module.app.model.PayDTO;
import com.alpha.module.app.service.PayService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("payService")
public class PayServiceImpl implements PayService {
    private PayInterface payInterface = null;

    public void setPayInterface(String payWay) {
        this.payInterface = PayFactory.create(payWay);
    }

    @Override
    public Map<String, String> payRequest(PayDTO model) {
        Map<String, String> map = new HashMap<>();
        setPayInterface(model.getPayWayName());
        payInterface.payRequest(model);
        map.put("appId", "");
        return map;
    }

    @Override
    public void pcPayRequest(PayDTO model) {
        setPayInterface(model.getPayWayName());
        payInterface.pcPay(model);
    }

    @Override
    public void h5PayRequest(PayDTO model) {
        setPayInterface(model.getPayWayName());
        payInterface.h5Pay(model);
    }

    @Override
    public void appPayRequest(PayDTO model) {
        payInterface.appPay(model);
    }

    @Override
    public void callBack(Map<String,String> map, String payWay) {
        setPayInterface(payWay);
        payInterface.callBack(map);
    }
}
