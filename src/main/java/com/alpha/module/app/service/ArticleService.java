package com.alpha.module.app.service;

import com.alpha.module.app.entity.Article;
import com.alpha.module.app.model.ArticleDTO;
import com.alpha.common.utils.PageUtils;

/**
 * article
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:11
 */
public interface ArticleService {

    /**
    * 获取列表
    * @param articleDTO
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(ArticleDTO articleDTO);

    /**
    * 保存
    */
    void save(Article article);

    /**
    * 根据id获取实例
     * @param id id
    */
    Article getById(Long id);


    /**
    * 更新
     */
    void updateById(Article article);

}

