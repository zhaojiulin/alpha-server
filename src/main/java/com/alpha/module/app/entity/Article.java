package com.alpha.module.app.entity;


import java.io.Serializable;
import java.util.Date;

/**
 * article
 * 
 * @author zhaojiulin
 * @date 2020-07-09 13:10:32
 */

public class Article implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK
	 */
	private Long id;
	/**
	 * 所属栏目
	 */
	private String nid;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 简介
	 */
	private String introduction;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 状态
	 */
	private Integer state;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 热文章
	 */
	private Integer isHot;
	/**
	 * 点击量
	 */
	private Integer clicks;
	/**
	 * 图片地址
	 */
	private String picPath;
	/**
	 * 链接
	 */
	private String url;
	/**
	 * 最后修改时间
	 */
	private Date updateTime;
	/**
	 * 商户id
	 */
	private Long merchantId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getIsHot() {
		return isHot;
	}

	public void setIsHot(Integer isHot) {
		this.isHot = isHot;
	}

	public Integer getClicks() {
		return clicks;
	}

	public void setClicks(Integer clicks) {
		this.clicks = clicks;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}
}
