package com.alpha.module.app.controller;

import com.alpha.module.app.model.PayDTO;
import com.alpha.module.app.service.PayService;
import com.alpha.common.utils.Enums;
import com.alpha.common.utils.HttpRequestUtils;
import com.alpha.common.utils.Res;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("alpha/pay")
public class PayController {
    private final PayService payService;

    public PayController(PayService payService) {
        this.payService = payService;
    }

    @RequestMapping("/aliPayCallBack")
    public Res aliPayCallBack(HttpServletRequest request) {
        payService.callBack(HttpRequestUtils.parseRequestMap(request), Enums.PayWays.AliPay.getValue());
        return Res.ok();
    }

    @RequestMapping("/unionPayCallBack")
    public Res unionPayCallBack(HttpServletRequest request) {
        payService.callBack(HttpRequestUtils.parseRequestMap(request), Enums.PayWays.UnionPay.getValue());
        return Res.ok();
    }

    @RequestMapping("/wxPayCallBack")
    public Res wxPayCallBack(HttpServletRequest request) {
        payService.callBack(HttpRequestUtils.parseRequestMap(request), Enums.PayWays.WxPay.getValue());
        return Res.ok();
    }

    @RequestMapping("/payRequest")
    public Res payRequest(@RequestBody PayDTO vo) {
        Map<String, String> payData = payService.payRequest(vo);
        return Res.ok("payData", payData);
    }

    @RequestMapping("/h5PayRequest")
    public Res h5PayRequest(@RequestBody PayDTO vo) {
        payService.h5PayRequest(vo);
        return Res.ok();
    }

    @RequestMapping("/pcPayRequest")
    public Res pcPayRequest(@RequestBody PayDTO vo) {
        payService.pcPayRequest(vo);
        return Res.ok();
    }

    @RequestMapping("/appPayRequest")
    public Res appPayRequest(@RequestBody PayDTO vo) {
        payService.appPayRequest(vo);
        return Res.ok();
    }
}
