package com.alpha.module.app.controller;


import com.alpha.module.app.entity.Article;
import com.alpha.module.app.model.ArticleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.alpha.module.app.service.ArticleService;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;

/**
 * article
 * 控制层
 * @author zhaojiulin
 * @date 2020-07-08 17:38:45
 */
@RestController
@RequestMapping("alpha/article")
public class ArticleController {
    private final ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody ArticleDTO articleDTO) {
        PageUtils page = articleService.queryPage(articleDTO);
        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		Article article = articleService.getById(id);

        return Res.ok().add("article", article);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody Article article){
		articleService.save(article);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody Article article){
		articleService.updateById(article);

        return Res.ok();
    }


}
