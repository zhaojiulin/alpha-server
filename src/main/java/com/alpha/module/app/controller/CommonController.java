package com.alpha.module.app.controller;

import com.alpha.module.app.model.SmsCodeDTO;
import com.alpha.module.app.model.UploadFileDTO;
import com.alpha.module.app.service.CommonService;
import com.alpha.common.utils.CommonUtils;
import com.alpha.common.utils.Constants;
import com.alpha.common.utils.Res;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("alpha/common")
public class CommonController {
    @Autowired
    HttpServletRequest request;

    private final CommonService commonService;

    public CommonController(CommonService commonService) {
        this.commonService = commonService;
    }

    /**
     * 发送短信验证码
     * @param smsCodeDTO 短信传输
     * @return success
     */
    @RequestMapping("/smsCode")
    public Res smsCode(@RequestBody SmsCodeDTO smsCodeDTO) {
        commonService.smsCode(smsCodeDTO);
        return Res.ok();
    }

    /**
     * 验证手机验证码
     * @param smsCodeDTO 短信传输包装
     * @return success
     */
    @RequestMapping("/verifySmsCode")
    public Res verifySmsCode(SmsCodeDTO smsCodeDTO) {
        commonService.verifySmsCode(smsCodeDTO);
        return Res.ok();
    }

    /**
     * 文件上传
     * @param file 文件包装类
     * @return success
     */
    @RequestMapping("/uploadPathFile")
    public Res uploadFile(@RequestBody UploadFileDTO file) {
        CommonUtils.setLocalData(Constants.MERCHANT_ID, file.getMerchantId());
        String s = commonService.uploadFile(file);
        return Res.ok("remoteUrl", s);
    }

    /**
     * 文件上传
     * @param file 文件包装类
     * @return success
     */
    @RequestMapping("/uploadMultipartFile")
    public Res uploadFile(@RequestParam("file") MultipartFile file) {
        String merchantId = request.getHeader(Constants.MERCHANT_ID);
        CommonUtils.setLocalData(Constants.MERCHANT_ID, merchantId);
        String url = commonService.uploadMultipartFile(file);
        return Res.ok("remoteUrl", url);
    }

    @RequestMapping("/uploadBase64File")
    public Res uploadFiles(@RequestBody UploadFileDTO fileVO) {
        CommonUtils.setLocalData(Constants.MERCHANT_ID, fileVO.getMerchantId());
        String filePath = commonService.uploadBase64File(fileVO);
        return Res.ok("remoteUrl", filePath);
    }
}
