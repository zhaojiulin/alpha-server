package com.alpha.module.app.model;

public class PayDataVO {
    private String payWayName;

    public String getPayWayName() {
        return payWayName;
    }

    public void setPayWayName(String payWayName) {
        this.payWayName = payWayName;
    }
}
