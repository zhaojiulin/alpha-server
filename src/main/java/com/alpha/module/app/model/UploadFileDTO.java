package com.alpha.module.app.model;


public class UploadFileDTO {
    // 绝对地址上传时 源文件地址
    private String originPath;
    // 目标上传地址
    private String targetPath;
    //  文件名称
    private String fileName;
    // 父文件夹地址 或前缀
    private String parentPath;
    // base64字符串
    private String base64;
    // 类型 1：源文件地址上传 2：base64字符串上传
    private Integer type;
    private Long merchantId;

    public String getOriginPath() {
        return originPath;
    }

    public void setOriginPath(String originPath) {
        this.originPath = originPath;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }
}
