package com.alpha.module.app.model;

/**
 * SmsCodeDTO
 * 短信验证码
 * @author zhaojiulin
 * @time 2020/6/30 3:53 下午
 */
public class SmsCodeDTO {
    private String mobile;
    private Integer type;
    private String code;

    private Long merchantId;

    public SmsCodeDTO() {

    }

    public SmsCodeDTO(String mobile, Integer type, String code) {
        this.mobile = mobile;
        this.type = type;
        this.code = code;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }
}