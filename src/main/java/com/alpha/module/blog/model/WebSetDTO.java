package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.WebSet;
import org.springframework.beans.BeanUtils;

public class WebSetDTO extends WebSet {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public WebSet property() {
        WebSet t = new WebSet();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public WebSetDTO instance(WebSet webSet) {
        if(webSet == null || webSet.getId() <= 0) {
            return null;
        }
        WebSetDTO webSetDTO = new WebSetDTO();
        BeanUtils.copyProperties(webSet,webSetDTO);
        return webSetDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
