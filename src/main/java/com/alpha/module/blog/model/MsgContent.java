package com.alpha.module.blog.model;

/**
 * MsgContent
 * 消息发送 包装
 * @author zhaojiulin
 * @time 2020/6/29 2:56 下午
 */
public class MsgContent {
    private String from;
    private String to;
    private String title;
    private String content;
    private String file;

    public MsgContent() {
    }

    public MsgContent(String from, String to, String title, String content) {
        this.from = from;
        this.to = to;
        this.title = title;
        this.content = content;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
