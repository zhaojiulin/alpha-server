package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.Tag;
import org.springframework.beans.BeanUtils;

public class TagDTO extends Tag {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public Tag property() {
        Tag t = new Tag();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public TagDTO instance(Tag tag) {
        if(tag == null || tag.getId() <= 0) {
            return null;
        }
        TagDTO tagDTO = new TagDTO();
        BeanUtils.copyProperties(tag,tagDTO);
        return tagDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
