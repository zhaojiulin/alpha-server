package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.BlogComment;
import org.springframework.beans.BeanUtils;

public class BlogCommentDTO extends BlogComment {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public BlogComment property() {
        BlogComment t = new BlogComment();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public BlogCommentDTO instance(BlogComment blogComment) {
        if(blogComment == null || blogComment.getId() <= 0) {
            return null;
        }
        BlogCommentDTO blogCommentDTO = new BlogCommentDTO();
        BeanUtils.copyProperties(blogComment,blogCommentDTO);
        return blogCommentDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
