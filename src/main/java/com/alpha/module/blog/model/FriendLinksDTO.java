package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.FriendLinks;
import org.springframework.beans.BeanUtils;

public class FriendLinksDTO extends FriendLinks {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public FriendLinks property() {
        FriendLinks t = new FriendLinks();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public FriendLinksDTO instance(FriendLinks friendLinks) {
        if(friendLinks == null || friendLinks.getId() <= 0) {
            return null;
        }
        FriendLinksDTO friendLinksDTO = new FriendLinksDTO();
        BeanUtils.copyProperties(friendLinks,friendLinksDTO);
        return friendLinksDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
