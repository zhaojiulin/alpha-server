package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.Blog;
import org.springframework.beans.BeanUtils;

public class BlogDTO extends Blog {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public Blog property() {
        Blog t = new Blog();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public BlogDTO instance(Blog blog) {
        if(blog == null || blog.getId() <= 0) {
            return null;
        }
        BlogDTO blogDTO = new BlogDTO();
        BeanUtils.copyProperties(blog,blogDTO);
        return blogDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
