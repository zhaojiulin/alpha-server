package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.Friend;
import org.springframework.beans.BeanUtils;

public class FriendDTO extends Friend {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public Friend property() {
        Friend t = new Friend();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public FriendDTO instance(Friend friend) {
        if(friend == null || friend.getId() <= 0) {
            return null;
        }
        FriendDTO friendDTO = new FriendDTO();
        BeanUtils.copyProperties(friend,friendDTO);
        return friendDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
