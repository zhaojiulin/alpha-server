package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.MsgLog;
import org.springframework.beans.BeanUtils;

public class MsgLogDTO extends MsgLog {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public MsgLog property() {
        MsgLog t = new MsgLog();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public MsgLogDTO instance(MsgLog msgLog) {
        if(msgLog == null || msgLog.getId() <= 0) {
            return null;
        }
        MsgLogDTO msgLogDTO = new MsgLogDTO();
        BeanUtils.copyProperties(msgLog,msgLogDTO);
        return msgLogDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
