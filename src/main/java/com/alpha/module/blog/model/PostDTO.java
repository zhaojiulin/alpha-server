package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.Post;
import org.springframework.beans.BeanUtils;

public class PostDTO extends Post {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public Post property() {
        Post t = new Post();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public PostDTO instance(Post post) {
        if(post == null || post.getId() <= 0) {
            return null;
        }
        PostDTO postDTO = new PostDTO();
        BeanUtils.copyProperties(post,postDTO);
        return postDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
