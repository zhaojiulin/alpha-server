package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.Forum;
import org.springframework.beans.BeanUtils;

public class ForumDTO extends Forum {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public Forum property() {
        Forum t = new Forum();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public ForumDTO instance(Forum forum) {
        if(forum == null || forum.getId() <= 0) {
            return null;
        }
        ForumDTO forumDTO = new ForumDTO();
        BeanUtils.copyProperties(forum,forumDTO);
        return forumDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
