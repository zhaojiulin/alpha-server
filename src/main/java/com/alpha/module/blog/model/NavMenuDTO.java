package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.NavMenu;
import org.springframework.beans.BeanUtils;

public class NavMenuDTO extends NavMenu {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public NavMenu property() {
        NavMenu t = new NavMenu();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public NavMenuDTO instance(NavMenu navMenu) {
        if(navMenu == null || navMenu.getId() <= 0) {
            return null;
        }
        NavMenuDTO navDTO = new NavMenuDTO();
        BeanUtils.copyProperties(navMenu,navDTO);
        return navDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
