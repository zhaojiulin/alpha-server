package com.alpha.module.blog.model;

import com.alpha.module.blog.entity.PostFloor;
import org.springframework.beans.BeanUtils;

public class PostFloorDTO extends PostFloor {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public PostFloor property() {
        PostFloor t = new PostFloor();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public PostFloorDTO instance(PostFloor postFloor) {
        if(postFloor == null || postFloor.getId() <= 0) {
            return null;
        }
        PostFloorDTO postFloorDTO = new PostFloorDTO();
        BeanUtils.copyProperties(postFloor,postFloorDTO);
        return postFloorDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
