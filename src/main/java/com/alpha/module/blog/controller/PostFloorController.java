package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.PostFloor;
import com.alpha.module.blog.model.PostFloorDTO;
import com.alpha.module.blog.service.PostFloorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 回帖楼层
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
@RestController
@RequestMapping("core/postfloor")
public class PostFloorController {
    @Autowired
    private PostFloorService postFloorService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody PostFloorDTO dto) {
        PageUtils page = postFloorService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		PostFloor postFloor = postFloorService.getById(id);

        return Res.ok().add("postFloor", postFloor);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody PostFloor postFloor){
		postFloorService.save(postFloor);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody PostFloor postFloor){
		postFloorService.updateById(postFloor);

        return Res.ok();
    }


}
