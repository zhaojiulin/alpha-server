package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.Friend;
import com.alpha.module.blog.model.FriendDTO;
import com.alpha.module.blog.service.FriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 好友
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
@RestController
@RequestMapping("core/friend")
public class FriendController {
    @Autowired
    private FriendService friendService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody FriendDTO dto) {
        PageUtils page = friendService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		Friend friend = friendService.getById(id);

        return Res.ok().add("friend", friend);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody Friend friend){
		friendService.save(friend);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody Friend friend){
		friendService.updateById(friend);

        return Res.ok();
    }


}
