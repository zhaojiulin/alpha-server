package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.Category;
import com.alpha.module.blog.model.CategoryDTO;
import com.alpha.module.blog.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 博客类别
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
@RestController
@RequestMapping("core/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody CategoryDTO dto) {
        PageUtils page = categoryService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		Category category = categoryService.getById(id);

        return Res.ok().add("category", category);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody Category category){
		categoryService.save(category);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody Category category){
		categoryService.updateById(category);

        return Res.ok();
    }


}
