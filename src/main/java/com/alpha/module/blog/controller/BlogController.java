package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.Blog;
import com.alpha.module.blog.model.BlogDTO;
import com.alpha.module.blog.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 博客文章
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:22:14
 */
@RestController
@RequestMapping("core/blog")
public class BlogController {
    @Autowired
    private BlogService blogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody BlogDTO dto) {
        PageUtils page = blogService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		Blog blog = blogService.getById(id);

        return Res.ok().add("mapper/app/blog", blog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody Blog blog){
		blogService.save(blog);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody Blog blog){
		blogService.updateById(blog);

        return Res.ok();
    }


}
