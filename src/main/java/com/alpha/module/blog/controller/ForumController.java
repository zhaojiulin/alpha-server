package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.Forum;
import com.alpha.module.blog.model.ForumDTO;
import com.alpha.module.blog.service.ForumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 论坛
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
@RestController
@RequestMapping("core/forum")
public class ForumController {
    @Autowired
    private ForumService forumService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody ForumDTO dto) {
        PageUtils page = forumService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		Forum forum = forumService.getById(id);

        return Res.ok().add("forum", forum);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody Forum forum){
		forumService.save(forum);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody Forum forum){
		forumService.updateById(forum);

        return Res.ok();
    }


}
