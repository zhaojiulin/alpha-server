package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.Tag;
import com.alpha.module.blog.model.TagDTO;
import com.alpha.module.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 标签
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
@RestController
@RequestMapping("core/tag")
public class TagController {
    @Autowired
    private TagService tagService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody TagDTO dto) {
        PageUtils page = tagService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		Tag tag = tagService.getById(id);

        return Res.ok().add("tag", tag);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody Tag tag){
		tagService.save(tag);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody Tag tag){
		tagService.updateById(tag);

        return Res.ok();
    }


}
