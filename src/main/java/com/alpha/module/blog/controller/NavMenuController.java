package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.NavMenu;
import com.alpha.module.blog.model.NavMenuDTO;
import com.alpha.module.blog.service.NavMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 网站导航
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
@RestController
@RequestMapping("core/nav")
public class NavMenuController {
    @Autowired
    private NavMenuService navMenuService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody NavMenuDTO dto) {
        PageUtils page = navMenuService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		NavMenu navMenu = navMenuService.getById(id);

        return Res.ok().add("nav", navMenu);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody NavMenu navMenu){
		navMenuService.save(navMenu);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody NavMenu navMenu){
		navMenuService.updateById(navMenu);

        return Res.ok();
    }


}
