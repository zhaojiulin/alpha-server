package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.FriendLinks;
import com.alpha.module.blog.model.FriendLinksDTO;
import com.alpha.module.blog.service.FriendLinksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 网站友链
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
@RestController
@RequestMapping("core/friendlinks")
public class FriendLinksController {
    @Autowired
    private FriendLinksService friendLinksService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody FriendLinksDTO dto) {
        PageUtils page = friendLinksService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		FriendLinks friendLinks = friendLinksService.getById(id);

        return Res.ok().add("friendLinks", friendLinks);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody FriendLinks friendLinks){
		friendLinksService.save(friendLinks);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody FriendLinks friendLinks){
		friendLinksService.updateById(friendLinks);

        return Res.ok();
    }


}
