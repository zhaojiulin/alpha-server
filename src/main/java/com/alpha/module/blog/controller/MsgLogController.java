package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.MsgLog;
import com.alpha.module.blog.model.MsgLogDTO;
import com.alpha.module.blog.service.MsgLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * ch_msg_log，消息投递日志
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
@RestController
@RequestMapping("core/msglog")
public class MsgLogController {
    @Autowired
    private MsgLogService msgLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody MsgLogDTO dto) {
        PageUtils page = msgLogService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		MsgLog msgLog = msgLogService.getById(id);

        return Res.ok().add("msgLog", msgLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody MsgLog msgLog){
		msgLogService.save(msgLog);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody MsgLog msgLog){
		msgLogService.updateById(msgLog);

        return Res.ok();
    }


}
