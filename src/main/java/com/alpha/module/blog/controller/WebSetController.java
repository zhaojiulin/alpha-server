package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.WebSet;
import com.alpha.module.blog.model.WebSetDTO;
import com.alpha.module.blog.service.WebSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 网站信息表
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
@RestController
@RequestMapping("core/webset")
public class WebSetController {
    @Autowired
    private WebSetService webSetService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody WebSetDTO dto) {
        PageUtils page = webSetService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		WebSet webSet = webSetService.getById(id);

        return Res.ok().add("webSet", webSet);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody WebSet webSet){
		webSetService.save(webSet);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody WebSet webSet){
		webSetService.updateById(webSet);

        return Res.ok();
    }


}
