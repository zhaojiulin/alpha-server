package com.alpha.module.blog.controller;


import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.blog.entity.BlogComment;
import com.alpha.module.blog.model.BlogCommentDTO;
import com.alpha.module.blog.service.BlogCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 文章评论
 *
 * @author zhaojiulin
 * @date 2020-07-15 15:22:14
 */
@RestController
@RequestMapping("core/blogcomment")
public class BlogCommentController {
    @Autowired
    private BlogCommentService blogCommentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody BlogCommentDTO dto) {
        PageUtils page = blogCommentService.queryPage(dto);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		BlogComment blogComment = blogCommentService.getById(id);

        return Res.ok().add("blogComment", blogComment);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody BlogComment blogComment){
		blogCommentService.save(blogComment);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody BlogComment blogComment){
		blogCommentService.updateById(blogComment);

        return Res.ok();
    }


}
