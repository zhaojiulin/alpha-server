package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.Friend;
import com.alpha.module.blog.model.FriendDTO;

/**
 * 好友
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface FriendService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(FriendDTO dto);

    /**
    * 保存
    */
    void save(Friend friend);

    /**
    * 根据id获取实例
     * @param id id
    */
    Friend getById(Long id);


    /**
    * 更新
     */
    void updateById(Friend friend);

}

