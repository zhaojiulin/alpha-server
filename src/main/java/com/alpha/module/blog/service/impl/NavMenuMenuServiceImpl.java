package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.NavMapper;
import com.alpha.module.blog.entity.NavMenu;
import com.alpha.module.blog.model.NavMenuDTO;
import com.alpha.module.blog.service.NavMenuService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("navService")
public class NavMenuMenuServiceImpl implements NavMenuService {

    @Autowired
    private NavMapper navMapper;

    @Override
    public PageUtils queryPage(NavMenuDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<NavMenu> list = this.navMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(NavMenu navMenu) {
            navMapper.insertSelective(navMenu);
    }

    @Override
    public NavMenu getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return navMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(NavMenu navMenu) {
            navMapper.updateByPrimaryKeySelective(navMenu);
    }


}