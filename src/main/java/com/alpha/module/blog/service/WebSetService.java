package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.WebSet;
import com.alpha.module.blog.model.WebSetDTO;

/**
 * 网站信息表
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface WebSetService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(WebSetDTO dto);

    /**
    * 保存
    */
    void save(WebSet webSet);

    /**
    * 根据id获取实例
     * @param id id
    */
    WebSet getById(Long id);


    /**
    * 更新
     */
    void updateById(WebSet webSet);

}

