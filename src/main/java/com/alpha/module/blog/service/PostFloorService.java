package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.PostFloor;
import com.alpha.module.blog.model.PostFloorDTO;

/**
 * 回帖楼层
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface PostFloorService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(PostFloorDTO dto);

    /**
    * 保存
    */
    void save(PostFloor postFloor);

    /**
    * 根据id获取实例
     * @param id id
    */
    PostFloor getById(Long id);


    /**
    * 更新
     */
    void updateById(PostFloor postFloor);

}

