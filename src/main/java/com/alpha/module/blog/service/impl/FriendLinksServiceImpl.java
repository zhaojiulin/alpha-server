package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.FriendLinksMapper;
import com.alpha.module.blog.entity.FriendLinks;
import com.alpha.module.blog.model.FriendLinksDTO;
import com.alpha.module.blog.service.FriendLinksService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("friendLinksService")
public class FriendLinksServiceImpl implements FriendLinksService {

    @Autowired
    private FriendLinksMapper friendLinksMapper;

    @Override
    public PageUtils queryPage(FriendLinksDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<FriendLinks> list = this.friendLinksMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(FriendLinks friendLinks) {
            friendLinksMapper.insertSelective(friendLinks);
    }

    @Override
    public FriendLinks getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return friendLinksMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(FriendLinks friendLinks) {
            friendLinksMapper.updateByPrimaryKeySelective(friendLinks);
    }


}