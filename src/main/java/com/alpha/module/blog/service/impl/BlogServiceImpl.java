package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.BlogMapper;
import com.alpha.module.blog.entity.Blog;
import com.alpha.module.blog.model.BlogDTO;
import com.alpha.module.blog.service.BlogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("blogService")
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogMapper blogMapper;

    @Override
    public PageUtils queryPage(BlogDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<Blog> list = this.blogMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public List<Blog> queryAllBlogList() {
        return this.blogMapper.queryAllBlogList();
    }

    @Override
    public void save(Blog blog) {
            blogMapper.insertSelective(blog);
    }

    @Override
    public Blog getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return blogMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(Blog blog) {
            blogMapper.updateByPrimaryKeySelective(blog);
    }


}