package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.Blog;
import com.alpha.module.blog.model.BlogDTO;

import java.util.List;

/**
 * 博客文章
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:22:14
 */
public interface BlogService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(BlogDTO dto);

    /**
    * 保存
    */
    void save(Blog blog);

    /**
    * 根据id获取实例
     * @param id id
    */
    Blog getById(Long id);


    /**
    * 更新
     */
    void updateById(Blog blog);

    /**
     * 查询全部
     * @return
     */
    List<Blog> queryAllBlogList();

}

