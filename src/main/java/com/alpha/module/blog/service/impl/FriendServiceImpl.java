package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.FriendMapper;
import com.alpha.module.blog.entity.Friend;
import com.alpha.module.blog.model.FriendDTO;
import com.alpha.module.blog.service.FriendService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("friendService")
public class FriendServiceImpl implements FriendService {

    @Autowired
    private FriendMapper friendMapper;

    @Override
    public PageUtils queryPage(FriendDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<Friend> list = this.friendMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(Friend friend) {
            friendMapper.insertSelective(friend);
    }

    @Override
    public Friend getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return friendMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(Friend friend) {
            friendMapper.updateByPrimaryKeySelective(friend);
    }


}