package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.Category;
import com.alpha.module.blog.model.CategoryDTO;

/**
 * 博客类别
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface CategoryService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(CategoryDTO dto);

    /**
    * 保存
    */
    void save(Category category);

    /**
    * 根据id获取实例
     * @param id id
    */
    Category getById(Long id);


    /**
    * 更新
     */
    void updateById(Category category);

}

