package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.PostMapper;
import com.alpha.module.blog.entity.Post;
import com.alpha.module.blog.model.PostDTO;
import com.alpha.module.blog.service.PostService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("postService")
public class PostServiceImpl implements PostService {

    @Autowired
    private PostMapper postMapper;

    @Override
    public PageUtils queryPage(PostDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<Post> list = this.postMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(Post post) {
            postMapper.insertSelective(post);
    }

    @Override
    public Post getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return postMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(Post post) {
            postMapper.updateByPrimaryKeySelective(post);
    }


}