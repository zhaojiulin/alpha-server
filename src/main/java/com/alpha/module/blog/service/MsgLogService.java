package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.MsgLog;
import com.alpha.module.blog.model.MsgLogDTO;

/**
 * ch_msg_log，消息投递日志
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface MsgLogService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(MsgLogDTO dto);

    /**
    * 保存
    */
    void save(MsgLog msgLog);

    /**
    * 根据id获取实例
     * @param id id
    */
    MsgLog getById(Long id);


    /**
    * 更新
     */
    void updateById(MsgLog msgLog);

}

