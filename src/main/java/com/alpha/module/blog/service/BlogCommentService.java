package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.BlogComment;
import com.alpha.module.blog.model.BlogCommentDTO;

/**
 * 文章评论
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:22:14
 */
public interface BlogCommentService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(BlogCommentDTO dto);

    /**
    * 保存
    */
    void save(BlogComment blogComment);

    /**
    * 根据id获取实例
     * @param id id
    */
    BlogComment getById(Long id);


    /**
    * 更新
     */
    void updateById(BlogComment blogComment);

}

