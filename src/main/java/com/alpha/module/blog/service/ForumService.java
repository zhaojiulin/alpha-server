package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.Forum;
import com.alpha.module.blog.model.ForumDTO;

/**
 * 论坛
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface ForumService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(ForumDTO dto);

    /**
    * 保存
    */
    void save(Forum forum);

    /**
    * 根据id获取实例
     * @param id id
    */
    Forum getById(Long id);


    /**
    * 更新
     */
    void updateById(Forum forum);

}

