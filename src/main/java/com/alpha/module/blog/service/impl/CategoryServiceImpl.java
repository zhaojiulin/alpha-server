package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.CategoryMapper;
import com.alpha.module.blog.entity.Category;
import com.alpha.module.blog.model.CategoryDTO;
import com.alpha.module.blog.service.CategoryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public PageUtils queryPage(CategoryDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<Category> list = this.categoryMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(Category category) {
            categoryMapper.insertSelective(category);
    }

    @Override
    public Category getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return categoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(Category category) {
            categoryMapper.updateByPrimaryKeySelective(category);
    }


}