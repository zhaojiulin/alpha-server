package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.ForumMapper;
import com.alpha.module.blog.entity.Forum;
import com.alpha.module.blog.model.ForumDTO;
import com.alpha.module.blog.service.ForumService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("forumService")
public class ForumServiceImpl implements ForumService {

    @Autowired
    private ForumMapper forumMapper;

    @Override
    public PageUtils queryPage(ForumDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<Forum> list = this.forumMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(Forum forum) {
            forumMapper.insertSelective(forum);
    }

    @Override
    public Forum getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return forumMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(Forum forum) {
            forumMapper.updateByPrimaryKeySelective(forum);
    }


}