package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.MsgLogMapper;
import com.alpha.module.blog.entity.MsgLog;
import com.alpha.module.blog.model.MsgLogDTO;
import com.alpha.module.blog.service.MsgLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("msgLogService")
public class MsgLogServiceImpl implements MsgLogService {

    @Autowired
    private MsgLogMapper msgLogMapper;

    @Override
    public PageUtils queryPage(MsgLogDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<MsgLog> list = this.msgLogMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(MsgLog msgLog) {
            msgLogMapper.insertSelective(msgLog);
    }

    @Override
    public MsgLog getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return msgLogMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(MsgLog msgLog) {
            msgLogMapper.updateByPrimaryKeySelective(msgLog);
    }


}