package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.BlogCommentMapper;
import com.alpha.module.blog.entity.BlogComment;
import com.alpha.module.blog.model.BlogCommentDTO;
import com.alpha.module.blog.service.BlogCommentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("blogCommentService")
public class BlogCommentServiceImpl implements BlogCommentService {

    @Autowired
    private BlogCommentMapper blogCommentMapper;

    @Override
    public PageUtils queryPage(BlogCommentDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<BlogComment> list = this.blogCommentMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(BlogComment blogComment) {
            blogCommentMapper.insertSelective(blogComment);
    }

    @Override
    public BlogComment getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return blogCommentMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(BlogComment blogComment) {
            blogCommentMapper.updateByPrimaryKeySelective(blogComment);
    }


}