package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.FriendLinks;
import com.alpha.module.blog.model.FriendLinksDTO;

/**
 * 网站友链
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface FriendLinksService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(FriendLinksDTO dto);

    /**
    * 保存
    */
    void save(FriendLinks friendLinks);

    /**
    * 根据id获取实例
     * @param id id
    */
    FriendLinks getById(Long id);


    /**
    * 更新
     */
    void updateById(FriendLinks friendLinks);

}

