package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.PostFloorMapper;
import com.alpha.module.blog.entity.PostFloor;
import com.alpha.module.blog.model.PostFloorDTO;
import com.alpha.module.blog.service.PostFloorService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("postFloorService")
public class PostFloorServiceImpl implements PostFloorService {

    @Autowired
    private PostFloorMapper postFloorMapper;

    @Override
    public PageUtils queryPage(PostFloorDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<PostFloor> list = this.postFloorMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(PostFloor postFloor) {
            postFloorMapper.insertSelective(postFloor);
    }

    @Override
    public PostFloor getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return postFloorMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(PostFloor postFloor) {
            postFloorMapper.updateByPrimaryKeySelective(postFloor);
    }


}