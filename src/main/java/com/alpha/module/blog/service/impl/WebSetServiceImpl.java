package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.WebSetMapper;
import com.alpha.module.blog.entity.WebSet;
import com.alpha.module.blog.model.WebSetDTO;
import com.alpha.module.blog.service.WebSetService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("webSetService")
public class WebSetServiceImpl implements WebSetService {

    @Autowired
    private WebSetMapper webSetMapper;

    @Override
    public PageUtils queryPage(WebSetDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<WebSet> list = this.webSetMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(WebSet webSet) {
            webSetMapper.insertSelective(webSet);
    }

    @Override
    public WebSet getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return webSetMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(WebSet webSet) {
            webSetMapper.updateByPrimaryKeySelective(webSet);
    }


}