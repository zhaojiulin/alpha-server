package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.Post;
import com.alpha.module.blog.model.PostDTO;

/**
 * 帖子
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface PostService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(PostDTO dto);

    /**
    * 保存
    */
    void save(Post post);

    /**
    * 根据id获取实例
     * @param id id
    */
    Post getById(Long id);


    /**
    * 更新
     */
    void updateById(Post post);

}

