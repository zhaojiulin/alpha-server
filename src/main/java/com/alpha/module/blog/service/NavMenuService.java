package com.alpha.module.blog.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.entity.NavMenu;
import com.alpha.module.blog.model.NavMenuDTO;

/**
 * 网站导航
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface NavMenuService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(NavMenuDTO dto);

    /**
    * 保存
    */
    void save(NavMenu navMenu);

    /**
    * 根据id获取实例
     * @param id id
    */
    NavMenu getById(Long id);


    /**
    * 更新
     */
    void updateById(NavMenu navMenu);

}

