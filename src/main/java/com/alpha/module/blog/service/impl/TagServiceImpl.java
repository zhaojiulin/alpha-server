package com.alpha.module.blog.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.blog.dao.TagMapper;
import com.alpha.module.blog.entity.Tag;
import com.alpha.module.blog.model.TagDTO;
import com.alpha.module.blog.service.TagService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("tagService")
public class TagServiceImpl implements TagService {

    @Autowired
    private TagMapper tagMapper;

    @Override
    public PageUtils queryPage(TagDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<Tag> list = this.tagMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(Tag tag) {
            tagMapper.insertSelective(tag);
    }

    @Override
    public Tag getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return tagMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(Tag tag) {
            tagMapper.updateByPrimaryKeySelective(tag);
    }


}