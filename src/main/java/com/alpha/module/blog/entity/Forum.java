package com.alpha.module.blog.entity;


import java.io.Serializable;
import java.util.Date;


/**
 * 论坛
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:56
 */

public class Forum implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 论坛名称 
	 */
	private String forumName;
	/**
	 * 论坛名称 
	 */
	private String forumDesc;
	/**
	 * 论坛logo 
	 */
	private String forumLogo;
	/**
	 * 论坛背景 
	 */
	private String forumBack;
	/**
	 * 帖子个数 
	 */
	private Integer postCount;
	/**
	 * 版主用户ID 
	 */
	private Long moderatorId;
	/**
	 * 父ID 
	 */
	private Long parentId;
	/**
	 * 状态 1：草稿 2：新建 -1：删除 
	 */
	private Integer state;
	/**
	 * 创建日期 
	 */
	private Date createTime;
	/**
	 * 更新时间 
	 */
	private Date modifyTime;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public String getForumName(){
        return forumName;
    }

    public void setForumName(String forumName) {
        this.forumName = forumName;
    }
	
	public String getForumDesc(){
        return forumDesc;
    }

    public void setForumDesc(String forumDesc) {
        this.forumDesc = forumDesc;
    }
	
	public String getForumLogo(){
        return forumLogo;
    }

    public void setForumLogo(String forumLogo) {
        this.forumLogo = forumLogo;
    }
	
	public String getForumBack(){
        return forumBack;
    }

    public void setForumBack(String forumBack) {
        this.forumBack = forumBack;
    }
	
	public Integer getPostCount(){
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }
	
	public Long getModeratorId(){
        return moderatorId;
    }

    public void setModeratorId(Long moderatorId) {
        this.moderatorId = moderatorId;
    }
	
	public Long getParentId(){
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
	
	public Integer getState(){
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
	
	public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	
	public Date getModifyTime(){
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

}
