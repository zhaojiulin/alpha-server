package com.alpha.module.blog.entity;


import java.io.Serializable;
import java.util.Date;


/**
 * 文章评论
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:22:14
 */

public class BlogComment implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 父评论ID 
	 */
	private Long parentId;
	/**
	 * 文章ID 
	 */
	private Long blogId;
	/**
	 * 评论者用户ID 
	 */
	private Long userId;
	/**
	 * 评论内容 
	 */
	private String content;
	/**
	 * 点赞数 
	 */
	private Integer likes;
	/**
	 * 状态 1：新建 -1：删除 
	 */
	private Integer state;
	/**
	 * 评论日期 
	 */
	private Date createTime;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public Long getParentId(){
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
	
	public Long getBlogId(){
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }
	
	public Long getUserId(){
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
	
	public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
	
	public Integer getLikes(){
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }
	
	public Integer getState(){
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
	
	public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
