package com.alpha.module.blog.entity;



import java.io.Serializable;


/**
 * 网站信息表
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:55
 */

public class WebSet implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * seo 网站title 
	 */
	private String title;
	/**
	 * 网站副标题 
	 */
	private String subtitle;
	/**
	 * seo 网站关键词 
	 */
	private String keywords;
	/**
	 * seo 网站描述 
	 */
	private String description;
	/**
	 * 网站ico logo 
	 */
	private String ico;
	/**
	 * 网站logo 
	 */
	private String logo;
	/**
	 * 网址 
	 */
	private String website;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public String getTitle(){
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
	
	public String getSubtitle(){
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
	
	public String getKeywords(){
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }
	
	public String getDescription(){
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
	
	public String getIco(){
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }
	
	public String getLogo(){
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
	
	public String getWebsite(){
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

}
