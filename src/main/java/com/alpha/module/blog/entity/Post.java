package com.alpha.module.blog.entity;


import java.io.Serializable;
import java.util.Date;


/**
 * 帖子
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:56
 */

public class Post implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 发帖者用户ID 
	 */
	private Long userId;
	/**
	 * 帖子ID 
	 */
	private Long forumId;
	/**
	 * 
	 */
	private String title;
	/**
	 * 帖子内容 
	 */
	private String content;
	/**
	 * 状态 1：草稿 2：新建 -1：删除 
	 */
	private Integer state;
	/**
	 * 浏览量 
	 */
	private Integer views;
	/**
	 * 回帖个数 
	 */
	private Integer replies;
	/**
	 * 创建时间 
	 */
	private Date createTime;
	/**
	 * 更新时间 
	 */
	private Date modifyTime;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public Long getUserId(){
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
	
	public Long getForumId(){
        return forumId;
    }

    public void setForumId(Long forumId) {
        this.forumId = forumId;
    }
	
	public String getTitle(){
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
	
	public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
	
	public Integer getState(){
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
	
	public Integer getViews(){
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }
	
	public Integer getReplies(){
        return replies;
    }

    public void setReplies(Integer replies) {
        this.replies = replies;
    }
	
	public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	
	public Date getModifyTime(){
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

}
