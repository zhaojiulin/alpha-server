package com.alpha.module.blog.entity;



import java.io.Serializable;


/**
 * 网站导航
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:56
 */

public class NavMenu implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 导航名称 
	 */
	private String navName;
	/**
	 * 导航链接 
	 */
	private String navLink;
	/**
	 * 导航分类 web_nav:pc端 mp_nav:小程序端 h5_nav:h5端 
	 */
	private String navCategory;
	/**
	 * 状态 1启用 -1：暂时禁用 -2：逻辑删除 
	 */
	private Integer state;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public String getNavName(){
        return navName;
    }

    public void setNavName(String navName) {
        this.navName = navName;
    }
	
	public String getNavLink(){
        return navLink;
    }

    public void setNavLink(String navLink) {
        this.navLink = navLink;
    }
	
	public String getNavCategory(){
        return navCategory;
    }

    public void setNavCategory(String navCategory) {
        this.navCategory = navCategory;
    }
	
	public Integer getState(){
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}
