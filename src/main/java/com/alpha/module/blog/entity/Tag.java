package com.alpha.module.blog.entity;



import java.io.Serializable;


/**
 * 标签
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:56
 */

public class Tag implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 标签名称 
	 */
	private String tagName;
	/**
	 * 标签别名 
	 */
	private String tagAlias;
	/**
	 * 描述 
	 */
	private String tagDescription;
	/**
	 * 状态 1：使用中 -1：禁用 -2： 删除 
	 */
	private Integer state;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public String getTagName(){
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
	
	public String getTagAlias(){
        return tagAlias;
    }

    public void setTagAlias(String tagAlias) {
        this.tagAlias = tagAlias;
    }
	
	public String getTagDescription(){
        return tagDescription;
    }

    public void setTagDescription(String tagDescription) {
        this.tagDescription = tagDescription;
    }
	
	public Integer getState(){
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}
