package com.alpha.module.blog.entity;



import java.io.Serializable;


/**
 * 博客类别
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:56
 */

public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 类别名称 
	 */
	private String categoryName;
	/**
	 * 别名 
	 */
	private String categoryAlias;
	/**
	 * 状态 1：使用中 -1：禁用 -2： 删除 
	 */
	private Integer state;
	/**
	 * 描述 
	 */
	private String description;
	/**
	 * 父ID 
	 */
	private Long parentId;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public String getCategoryName(){
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
	
	public String getCategoryAlias(){
        return categoryAlias;
    }

    public void setCategoryAlias(String categoryAlias) {
        this.categoryAlias = categoryAlias;
    }
	
	public Integer getState(){
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
	
	public String getDescription(){
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
	
	public Long getParentId(){
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

}
