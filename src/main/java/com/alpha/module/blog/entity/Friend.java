package com.alpha.module.blog.entity;



import java.io.Serializable;


/**
 * 好友
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:55
 */

public class Friend implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 用户ID 
	 */
	private Long userId;
	/**
	 * 好友ID 
	 */
	private Long friendId;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public Long getUserId(){
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
	
	public Long getFriendId(){
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

}
