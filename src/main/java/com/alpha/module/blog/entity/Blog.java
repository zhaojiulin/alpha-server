package com.alpha.module.blog.entity;


import java.io.Serializable;
import java.util.Date;


/**
 * 博客文章
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:22:14
 */

public class Blog implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 发布者用户ID 
	 */
	private Long userId;
	/**
	 * 类别ID 
	 */
	private Long categoryId;
	/**
	 * 博文标题 
	 */
	private String title;
	/**
	 * 标签 
	 */
	private String tags;
	/**
	 * 博文内容 。正文 
	 */
	private String content;
	/**
	 * 点赞数 
	 */
	private Integer likes;
	/**
	 * 浏览量 
	 */
	private Integer views;
	/**
	 * 回复数 
	 */
	private Integer replies;
	/**
	 * 状态 1：草稿 2：新建 3：展示中 -1：删除 
	 */
	private Integer state;
	/**
	 * 发布日期 
	 */
	private Date createTime;
	/**
	 * 更新时间 
	 */
	private Date modifyTime;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public Long getUserId(){
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
	
	public Long getCategoryId(){
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
	
	public String getTitle(){
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
	
	public String getTags(){
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
	
	public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
	
	public Integer getLikes(){
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }
	
	public Integer getViews(){
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }
	
	public Integer getReplies(){
        return replies;
    }

    public void setReplies(Integer replies) {
        this.replies = replies;
    }
	
	public Integer getState(){
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
	
	public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	
	public Date getModifyTime(){
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

}
