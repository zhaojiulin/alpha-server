package com.alpha.module.blog.entity;



import java.io.Serializable;


/**
 * 网站友链
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:55
 */

public class FriendLinks implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * logo 
	 */
	private String linkLogo;
	/**
	 * 友链 合作伙伴 
	 */
	private String linkName;
	/**
	 * 链接 
	 */
	private String linkHref;
	/**
	 * 描述 
	 */
	private String linkDesc;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public String getLinkLogo(){
        return linkLogo;
    }

    public void setLinkLogo(String linkLogo) {
        this.linkLogo = linkLogo;
    }
	
	public String getLinkName(){
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }
	
	public String getLinkHref(){
        return linkHref;
    }

    public void setLinkHref(String linkHref) {
        this.linkHref = linkHref;
    }
	
	public String getLinkDesc(){
        return linkDesc;
    }

    public void setLinkDesc(String linkDesc) {
        this.linkDesc = linkDesc;
    }

}
