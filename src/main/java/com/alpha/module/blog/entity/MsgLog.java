package com.alpha.module.blog.entity;


import java.io.Serializable;
import java.util.Date;


/**
 * ch_msg_log，消息投递日志
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:56
 */

public class MsgLog implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 标题 
	 */
	private String title;
	/**
	 * 消息体,
    json格式化 
	 */
	private String msg;
	/**
	 * 交换机 
	 */
	private String exchange;
	/**
	 * 路由 邮件 短信 站内信 
	 */
	private String routingKey;
	/**
	 * 状态: 0发送中 1发送成功 2发送失败 3已消费/已读 
	 */
	private Integer state;
	/**
	 * 重试次数 
	 */
	private Integer tryCount;
	/**
	 * 下一次重试时间 
	 */
	private Date nextTryTime;
	/**
	 * 创建时间 
	 */
	private Date createTime;
	/**
	 * 更新时间 
	 */
	private Date modifyTime;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public String getTitle(){
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
	
	public String getMsg(){
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
	
	public String getExchange(){
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }
	
	public String getRoutingKey(){
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }
	
	public Integer getState(){
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
	
	public Integer getTryCount(){
        return tryCount;
    }

    public void setTryCount(Integer tryCount) {
        this.tryCount = tryCount;
    }
	
	public Date getNextTryTime(){
        return nextTryTime;
    }

    public void setNextTryTime(Date nextTryTime) {
        this.nextTryTime = nextTryTime;
    }
	
	public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	
	public Date getModifyTime(){
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

}
