package com.alpha.module.blog.entity;


import java.io.Serializable;
import java.util.Date;


/**
 * 回帖楼层
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:56
 */

public class PostFloor implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 回帖用户ID 
	 */
	private Long userId;
	/**
	 * 主贴ID 
	 */
	private Long postId;
	/**
	 * 父ID 
	 */
	private Long parentId;
	/**
	 * 回帖内容 
	 */
	private String content;
	/**
	 * 楼层 
	 */
	private Integer floor;
	/**
	 * 状态 1：新建 -1：删除 
	 */
	private Integer state;
	/**
	 * 回帖时间 
	 */
	private Date createTime;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public Long getUserId(){
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
	
	public Long getPostId(){
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }
	
	public Long getParentId(){
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
	
	public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
	
	public Integer getFloor(){
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }
	
	public Integer getState(){
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
	
	public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
