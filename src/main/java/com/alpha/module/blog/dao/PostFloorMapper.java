package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.PostFloor;
import com.alpha.module.blog.model.PostFloorDTO;

import java.util.List;

/**
 * 回帖楼层
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface PostFloorMapper  {

    List<PostFloor> queryList(PostFloorDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(PostFloor record);

    int insertSelective(PostFloor record);

    PostFloor selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PostFloor record);

    int updateByPrimaryKey(PostFloor record);
	
}
