package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.NavMenu;
import com.alpha.module.blog.model.NavMenuDTO;

import java.util.List;

/**
 * 网站导航
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface NavMapper  {

    List<NavMenu> queryList(NavMenuDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(NavMenu record);

    int insertSelective(NavMenu record);

    NavMenu selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(NavMenu record);

    int updateByPrimaryKey(NavMenu record);
	
}
