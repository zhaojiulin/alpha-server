package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.Category;
import com.alpha.module.blog.model.CategoryDTO;

import java.util.List;

/**
 * 博客类别
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface CategoryMapper  {

    List<Category> queryList(CategoryDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(Category record);

    int insertSelective(Category record);

    Category selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);
	
}
