package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.WebSet;
import com.alpha.module.blog.model.WebSetDTO;

import java.util.List;

/**
 * 网站信息表
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface WebSetMapper  {

    List<WebSet> queryList(WebSetDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(WebSet record);

    int insertSelective(WebSet record);

    WebSet selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WebSet record);

    int updateByPrimaryKey(WebSet record);
	
}
