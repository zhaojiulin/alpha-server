package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.MsgLog;
import com.alpha.module.blog.model.MsgLogDTO;

import java.util.List;

/**
 * ch_msg_log，消息投递日志
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface MsgLogMapper  {

    List<MsgLog> queryList(MsgLogDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(MsgLog record);

    int insertSelective(MsgLog record);

    MsgLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(MsgLog record);

    int updateByPrimaryKey(MsgLog record);
	
}
