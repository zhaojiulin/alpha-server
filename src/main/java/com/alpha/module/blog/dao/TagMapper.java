package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.Tag;
import com.alpha.module.blog.model.TagDTO;

import java.util.List;

/**
 * 标签
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface TagMapper  {

    List<Tag> queryList(TagDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(Tag record);

    int insertSelective(Tag record);

    Tag selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Tag record);

    int updateByPrimaryKey(Tag record);
	
}
