package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.BlogComment;
import com.alpha.module.blog.model.BlogCommentDTO;

import java.util.List;

/**
 * 文章评论
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:22:14
 */
public interface BlogCommentMapper  {

    List<BlogComment> queryList(BlogCommentDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(BlogComment record);

    int insertSelective(BlogComment record);

    BlogComment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BlogComment record);

    int updateByPrimaryKey(BlogComment record);
	
}
