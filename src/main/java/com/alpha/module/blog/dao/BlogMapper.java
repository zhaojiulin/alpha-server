package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.Blog;
import com.alpha.module.blog.model.BlogDTO;

import java.util.List;

/**
 * 博客文章
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:22:14
 */
public interface BlogMapper  {

    List<Blog> queryList(BlogDTO params);

    List<Blog> queryAllBlogList();

    int deleteByPrimaryKey(Long id);

    int insert(Blog record);

    int insertSelective(Blog record);

    Blog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Blog record);

    int updateByPrimaryKey(Blog record);
	
}
