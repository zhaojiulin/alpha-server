package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.FriendLinks;
import com.alpha.module.blog.model.FriendLinksDTO;

import java.util.List;

/**
 * 网站友链
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface FriendLinksMapper  {

    List<FriendLinks> queryList(FriendLinksDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(FriendLinks record);

    int insertSelective(FriendLinks record);

    FriendLinks selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(FriendLinks record);

    int updateByPrimaryKey(FriendLinks record);
	
}
