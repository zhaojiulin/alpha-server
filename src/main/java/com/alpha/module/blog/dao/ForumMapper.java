package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.Forum;
import com.alpha.module.blog.model.ForumDTO;

import java.util.List;

/**
 * 论坛
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface ForumMapper  {

    List<Forum> queryList(ForumDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(Forum record);

    int insertSelective(Forum record);

    Forum selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Forum record);

    int updateByPrimaryKey(Forum record);
	
}
