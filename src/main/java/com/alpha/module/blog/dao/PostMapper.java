package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.Post;
import com.alpha.module.blog.model.PostDTO;

import java.util.List;

/**
 * 帖子
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface PostMapper  {

    List<Post> queryList(PostDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(Post record);

    int insertSelective(Post record);

    Post selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Post record);

    int updateByPrimaryKey(Post record);
	
}
