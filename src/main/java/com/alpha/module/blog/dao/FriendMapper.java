package com.alpha.module.blog.dao;

import com.alpha.module.blog.entity.Friend;
import com.alpha.module.blog.model.FriendDTO;

import java.util.List;

/**
 * 好友
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:38
 */
public interface FriendMapper  {

    List<Friend> queryList(FriendDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(Friend record);

    int insertSelective(Friend record);

    Friend selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Friend record);

    int updateByPrimaryKey(Friend record);
	
}
