package com.alpha.module.user.model;

import com.alpha.module.user.entity.User;
import org.springframework.beans.BeanUtils;

public class UserDTO extends User {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public User property() {
        User t = new User();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public static UserDTO instance(User user) {
        if(user == null || user.getId() <= 0) {
            return null;
        }
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        return userDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
