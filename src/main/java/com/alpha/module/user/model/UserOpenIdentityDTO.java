package com.alpha.module.user.model;

import com.alpha.module.user.entity.UserOpenIdentity;
import org.springframework.beans.BeanUtils;

public class UserOpenIdentityDTO extends UserOpenIdentity {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public UserOpenIdentity property() {
        UserOpenIdentity t = new UserOpenIdentity();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public UserOpenIdentityDTO instance(UserOpenIdentity userOpenIdentity) {
        if(userOpenIdentity == null || userOpenIdentity.getId() <= 0) {
            return null;
        }
        UserOpenIdentityDTO userOpenIdentityDTO = new UserOpenIdentityDTO();
        BeanUtils.copyProperties(userOpenIdentity, userOpenIdentityDTO);
        return userOpenIdentityDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
