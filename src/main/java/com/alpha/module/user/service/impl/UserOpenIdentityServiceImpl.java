package com.alpha.module.user.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.app.service.CommonService;
import com.alpha.module.user.dao.UserOpenIdentityMapper;
import com.alpha.module.user.entity.UserOpenIdentity;
import com.alpha.module.user.model.UserOpenIdentityDTO;
import com.alpha.module.user.service.UserOpenIdentityService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("userOpenIdentityService")
public class UserOpenIdentityServiceImpl implements UserOpenIdentityService {

    @Autowired
    private UserOpenIdentityMapper userOpenIdentityMapper;

    @Autowired
    private CommonService commonService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        List<UserOpenIdentity> list = null;
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(UserOpenIdentity userOpenIdentity) {
            userOpenIdentityMapper.insertSelective(userOpenIdentity);
    }

    @Override
    public UserOpenIdentity getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return userOpenIdentityMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(UserOpenIdentity userOpenIdentity) {
        userOpenIdentityMapper.updateByPrimaryKeySelective(userOpenIdentity);
    }

    @Transactional
    @Override
    public void saveByUserId(Long userId, String identityType, String identifier) {
        UserOpenIdentity userOpenIdentity = this.userOpenIdentityMapper.selectByIdentityType(identityType, identifier);
        if (userOpenIdentity == null) {
            userOpenIdentity = new UserOpenIdentity();
            userOpenIdentity.setUserId(userId);
            userOpenIdentity.setCreateTime(new Date());
            userOpenIdentity.setIdentityType(identityType);
            userOpenIdentity.setIdentifier(identifier);
            this.userOpenIdentityMapper.insertSelective(userOpenIdentity);
        }
    }

    @Override
    public UserOpenIdentity selectByIdentityType(String identityType, String identifier) {
        return null;
    }

    @Override
    public UserOpenIdentity loginByCredential(UserOpenIdentityDTO openVo) {
        return this.userOpenIdentityMapper.selectByIdentifierCredential(openVo.getIdentityType(), openVo.getIdentifier(), openVo.getCredential());
    }

}