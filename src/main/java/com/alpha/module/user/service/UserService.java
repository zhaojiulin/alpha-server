package com.alpha.module.user.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.app.model.SmsCodeDTO;
import com.alpha.module.user.entity.User;
import com.alpha.module.user.model.UserOpenIdentityDTO;

import java.util.Map;

/**
 * 用户表：ch_user
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:11
 */
public interface UserService {

    /**
    * 获取列表
    * @param params
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(Map<String, Object> params);

    /**
    * 保存
    */
    void save(User user);

    /**
    * 根据id获取实例
     * @param id id
    */
    User getById(Long id);


    /**
    * 更新
     */
    void updateById(User user);

    /**
     * @手机号登录注册
     */
    User mobileCodeLogin(SmsCodeDTO smsCodeDTO);

    User login(UserOpenIdentityDTO dto);

}

