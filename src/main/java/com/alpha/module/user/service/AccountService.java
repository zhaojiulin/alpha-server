package com.alpha.module.user.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.user.entity.Account;
import com.alpha.module.user.entity.User;

import java.util.Map;

/**
 * 账户资产表：account
 * dao
 * @author zhaojiulin
 * @date 2020-07-09 15:00:25
 */
public interface AccountService {

    /**
    * 获取列表
    * @param params
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(Map<String, Object> params);

    /**
    * 保存
    */
    void save(Account account);

    /**
    * 根据id获取实例
     * @param id id
    */
    Account getById(Long id);


    /**
    * 更新
     */
    void updateById(Account account);

    /**
     * 用户生成账户
     * @param user 用户信息
     */
    void saveByUser(User user);

}

