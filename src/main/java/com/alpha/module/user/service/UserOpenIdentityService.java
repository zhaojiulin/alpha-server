package com.alpha.module.user.service;

import com.alpha.common.utils.PageUtils;
import com.alpha.module.user.entity.UserOpenIdentity;
import com.alpha.module.user.model.UserOpenIdentityDTO;

import java.util.Map;

/**
 * 第三方登录授权表：ch_user_open_identity
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:11
 */
public interface UserOpenIdentityService {

    /**
    * 获取列表
    * @param params
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(Map<String, Object> params);

    /**
    * 保存
    */
    void save(UserOpenIdentity userOpenIdentity);

    /**
    * 根据id获取实例
     * @param id id
    */
    UserOpenIdentity getById(Long id);


    /**
    * 更新
     */
    void updateById(UserOpenIdentity userOpenIdentity);

    /**
     * 注册
     */
    void saveByUserId(Long userId, String identityType, String identifier);

    /**
     *
     * 根据登录类型和登录账号查询石佛存在
     */
    UserOpenIdentity selectByIdentityType(String identityType, String identifier);

    /**
     * 凭证登录
     */
    UserOpenIdentity loginByCredential(UserOpenIdentityDTO openVo);

}

