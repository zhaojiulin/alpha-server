package com.alpha.module.user.service.impl;

import com.alpha.common.utils.*;
import com.alpha.module.app.model.SmsCodeDTO;
import com.alpha.module.app.service.CommonService;
import com.alpha.module.user.dao.UserMapper;
import com.alpha.module.user.entity.User;
import com.alpha.module.user.entity.UserOpenIdentity;
import com.alpha.module.user.model.UserOpenIdentityDTO;
import com.alpha.module.user.service.AccountService;
import com.alpha.module.user.service.UserOpenIdentityService;
import com.alpha.module.user.service.UserService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;


@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private UserOpenIdentityService userOpenIdentityService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        List<User> list = null;
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(User user) {
            userMapper.insertSelective(user);
    }

    @Override
    public User getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(User user) {
        userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public User login(UserOpenIdentityDTO openIdentity) {
        if (Objects.equals(openIdentity.getIdentityType(), Enums.IdentityType.MOBILE_CODE.getValue())) {
            this.commonService.verifySmsCode(new SmsCodeDTO(openIdentity.getIdentifier(), 1, openIdentity.getCredential()));
            User user = this.userMapper.selectByMobile(openIdentity.getIdentifier());
            if (user == null) {
                user = new User();
                user.setState(1);
                user.setMobile(openIdentity.getIdentifier());
                user.setCreateTime(DateTimeUtil.zoneNowDate());
                user.setUsername(StrUtils.generateNonceStr("m", 8));
                user.setMerchantId(1L);
                this.userMapper.insertSelective(user);
                this.userOpenIdentityService.saveByUserId(user.getId(), openIdentity.getIdentifier(), openIdentity.getCredential());
                this.accountService.saveByUser(user);
            }
            return user;
        } else {
            UserOpenIdentity userOpenIdentity = this.userOpenIdentityService.loginByCredential(openIdentity);
            if (userOpenIdentity != null) {
                return this.userMapper.selectByPrimaryKey(userOpenIdentity.getUserId());
            }
            return null;
        }
    }

    @Transactional
    @Override
    public User mobileCodeLogin(SmsCodeDTO smsCodeDTO) {

        return null;
    }

}