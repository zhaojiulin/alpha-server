package com.alpha.module.user.controller;

import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.user.entity.Account;
import com.alpha.module.user.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 账户资产表：account
 *
 * @author zhaojiulin
 * @date 2020-07-09 15:00:25
 */
@RestController
@RequestMapping("alpha/account")
public class AccountController {
    @Autowired
    private AccountService accountService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestParam Map<String, Object> params) {
        PageUtils page = accountService.queryPage(params);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		Account account = accountService.getById(id);

        return Res.ok().add("account", account);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody Account account){
		accountService.save(account);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody Account account){
		accountService.updateById(account);

        return Res.ok();
    }


}
