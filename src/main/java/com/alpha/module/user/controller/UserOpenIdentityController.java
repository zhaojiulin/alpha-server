package com.alpha.module.user.controller;

import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.user.entity.UserOpenIdentity;
import com.alpha.module.user.model.UserOpenIdentityDTO;
import com.alpha.module.user.service.UserOpenIdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 第三方登录授权表：user_open_identity
 *
 * @author zhaojiulin
 * @date 2020-07-08 17:38:44
 */
@RestController
@RequestMapping("alpha/useropenidentity")
public class UserOpenIdentityController {
    @Autowired
    private UserOpenIdentityService userOpenIdentityService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestParam Map<String, Object> params) {
        PageUtils page = userOpenIdentityService.queryPage(params);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		UserOpenIdentity userOpenIdentity = userOpenIdentityService.getById(id);

        return Res.ok().add("userOpenIdentity", userOpenIdentity);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody UserOpenIdentity userOpenIdentity){
		userOpenIdentityService.save(userOpenIdentity);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody UserOpenIdentity userOpenIdentity){
		userOpenIdentityService.updateById(userOpenIdentity);

        return Res.ok();
    }

    /**
     * 登录
     */
    @RequestMapping("/login")
    public Res login(UserOpenIdentityDTO identityVO){
        UserOpenIdentity openIdentity = this.userOpenIdentityService.loginByCredential(identityVO);
        return Res.ok().add("userIdentity", openIdentity);
    }


}
