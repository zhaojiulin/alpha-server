package com.alpha.module.user.controller;

import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import com.alpha.module.app.model.SmsCodeDTO;
import com.alpha.module.user.entity.User;
import com.alpha.module.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * 用户表：ch_user
 *
 * @author zhaojiulin
 * @date 2020-07-08 17:38:44
 */
@RestController
@RequestMapping("alpha/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private HttpServletRequest request;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestParam Map<String, Object> params) {
        PageUtils page = userService.queryPage(params);

        return Res.ok().add(page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		User user = userService.getById(id);

        return Res.ok().add("user", user);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody User user){
		userService.save(user);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody User user){
		userService.updateById(user);

        return Res.ok();
    }

    /**
     * 手机号登录注册
     */
    @RequestMapping("/login")
    public Res login(@RequestBody SmsCodeDTO smsCodeDTO){
		User user = userService.mobileCodeLogin(smsCodeDTO);
        return Res.ok("user", user);
    }


}
