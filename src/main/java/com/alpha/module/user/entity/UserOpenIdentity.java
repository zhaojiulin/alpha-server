package com.alpha.module.user.entity;


import java.io.Serializable;
import java.util.Date;

/**
 * 第三方登录授权表：ch_user_open_identity
 * 
 * @author zhaojiulin
 * @date 2020-07-09 13:10:31
 */

public class UserOpenIdentity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
	private Long id;
	/**
	 * 用户id 
	 */
	private Long userId;
	/**
	 * 授权类型 qq、mobile、email、weixin、weibo。。。
	 */
	private String identityType;
	/**
	 * 标识 (手机号/邮箱/用户名或第三方应用的唯一标识)
	 */
	private String identifier;
	/**
	 * 登录凭证 
	 */
	private String credential;
	/**
	 * 授权日期 
	 */
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getCredential() {
		return credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
