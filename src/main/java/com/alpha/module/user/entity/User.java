package com.alpha.module.user.entity;


import java.io.Serializable;
import java.util.Date;

/**
 * 用户表：ch_user
 * 
 * @author zhaojiulin
 * @date 2020-07-09 13:10:31
 */

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
	private Long id;
	/**
	 * 用户名 
	 */
	private String username;
	/**
	 * 手机号 
	 */
	private String mobile;
	/**
	 * 邮箱 
	 */
	private String email;
	/**
	 * 头像 
	 */
	private String avatar;
	/**
	 * 状态 
	 */
	private Integer state;
	/**
	 * 商户id
	 */
	private Long merchantId;
	/**
	 * 注册时间 
	 */
	private Date createTime;
	/**
	 * 邀请人
	 */
	private String inviteCode;

	/**
	 * 背景图
	 */
	private String backgroundImg;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

	public String getBackgroundImg() {
		return backgroundImg;
	}

	public void setBackgroundImg(String backgroundImg) {
		this.backgroundImg = backgroundImg;
	}
}
