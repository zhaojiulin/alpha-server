package com.alpha.module.user.entity;


import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 账户资产表：account
 * 
 * @author zhaojiulin
 * @date 2020-07-09 15:00:25
 */

public class Account implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
	private Long id;
	/**
	 * 用户 
	 */
	private Long userId;
	/**
	 * 账户总金额 
	 */
	private BigDecimal totalAmount;
	/**
	 * 可用余额 
	 */
	private BigDecimal balance;
	/**
	 * 可用积分 
	 */
	private Integer integral;
	/**
	 * 版本控制 
	 */
	private String version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Integer getIntegral() {
		return integral;
	}

	public void setIntegral(Integer integral) {
		this.integral = integral;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
