package com.alpha.module.user.dao;

import com.alpha.module.user.entity.UserOpenIdentity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

public interface UserOpenIdentityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserOpenIdentity record);

    int insertSelective(UserOpenIdentity record);

    UserOpenIdentity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserOpenIdentity record);

    int updateByPrimaryKey(UserOpenIdentity record);

    @Select({"select * from user_open_identity where identity_type= #{identityType,jdbcType=VARCHAR} and identifier= #{identifier,jdbcType=VARCHAR}"})
    @ResultMap({"BaseResultMap"})
    UserOpenIdentity selectByIdentityType(@Param("identityType") String identityType, @Param("identifier") String identifier);

    /**
     * 类型 标识 凭证查询
     * @param identityType 身份类型
     * @param identifier 身份账号
     * @param credential 凭证
     * @return
     */
    @Select({"select * from user_open_identity where identity_type= #{identityType,jdbcType=VARCHAR} and identifier= #{identifier,jdbcType=VARCHAR} and credential= #{credential,jdbcType=VARCHAR}"})
    @ResultMap({"BaseResultMap"})
    UserOpenIdentity selectByIdentifierCredential(@Param("identityType") String identityType, @Param("identifier") String identifier, @Param("credential") String credential);
}