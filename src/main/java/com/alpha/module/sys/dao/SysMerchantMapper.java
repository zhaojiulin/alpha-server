package com.alpha.module.sys.dao;

import com.alpha.module.sys.entity.SysMerchant;
import com.alpha.module.sys.model.SysMerchantDTO;

import java.util.List;

public interface SysMerchantMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysMerchant record);

    int insertSelective(SysMerchant record);

    SysMerchant selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysMerchant record);

    int updateByPrimaryKey(SysMerchant record);

    List<SysMerchant> queryList(SysMerchantDTO vo);

    long selectMax();

    SysMerchant selectByDomainName(String domainName);
}