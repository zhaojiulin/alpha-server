package com.alpha.module.sys.dao;

import com.alpha.module.sys.entity.SysConfig;
import com.alpha.module.sys.model.SysConfigDTO;

import java.util.List;

public interface SysConfigMapper {
    List<SysConfig> queryList(SysConfigDTO vo);

    int deleteByPrimaryKey(Long id);

    int insert(SysConfig record);

    int insertSelective(SysConfig record);

    SysConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysConfig record);

    int updateByPrimaryKey(SysConfig record);

    SysConfig selectByDomainName(String domainName);

    List<SysConfig> queryAll();
}