package com.alpha.module.sys.dao;

import com.alpha.module.sys.entity.SysRole;
import com.alpha.module.sys.model.SysRoleDTO;

import java.util.HashMap;
import java.util.List;

public interface SysRoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

    List<HashMap> queryJoinList(SysRoleDTO vo);

    List<SysRole> queryList(SysRoleDTO vo);
}