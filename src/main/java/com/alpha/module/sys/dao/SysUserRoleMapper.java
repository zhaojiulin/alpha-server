package com.alpha.module.sys.dao;

import com.alpha.module.sys.entity.SysUserRole;

import java.util.List;

public interface SysUserRoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);

    List<Long> selectRoleIds(Long userId);

    List<SysUserRole> selectRolesByUserId(Long userId);

    int deleteByUserId(Long userId);
}