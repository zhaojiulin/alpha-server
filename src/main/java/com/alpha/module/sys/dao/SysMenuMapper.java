package com.alpha.module.sys.dao;

import com.alpha.module.sys.entity.SysMenu;
import com.alpha.module.sys.model.SysMenuDTO;

import java.util.List;

public interface SysMenuMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    SysMenu selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysMenu record);

    int updateByPrimaryKey(SysMenu record);

    List<SysMenu> selectAllMenu();

    List<SysMenu> queryList(SysMenuDTO vo);
}