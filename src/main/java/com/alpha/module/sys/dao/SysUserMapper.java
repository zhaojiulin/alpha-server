package com.alpha.module.sys.dao;

import com.alpha.module.sys.entity.SysUser;
import com.alpha.module.sys.model.SysUserDTO;

import java.util.List;

public interface SysUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    SysUser selectByCredential(SysUserDTO vo);

    List<SysUserDTO> queryJoinList(SysUserDTO vo);

    List<SysUser> queryList(SysUserDTO vo);
}