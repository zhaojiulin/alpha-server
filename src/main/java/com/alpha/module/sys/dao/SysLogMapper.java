package com.alpha.module.sys.dao;

import com.alpha.module.sys.entity.SysLog;
import com.alpha.module.sys.model.SysLogDTO;

import java.util.List;

/**
 * 操作日志表
 * 
 * @author zhaojiulin
 * @date 2020-07-15 15:03:37
 */
public interface SysLogMapper {

    List<SysLog> queryList(SysLogDTO params);

    int deleteByPrimaryKey(Long id);

    int insert(SysLog record);

    int insertSelective(SysLog record);

    SysLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysLog record);

    int updateByPrimaryKey(SysLog record);
	
}
