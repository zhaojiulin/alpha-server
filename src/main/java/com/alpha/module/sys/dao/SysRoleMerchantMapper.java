package com.alpha.module.sys.dao;

import com.alpha.module.sys.entity.SysRoleMerchant;

public interface SysRoleMerchantMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysRoleMerchant record);

    int insertSelective(SysRoleMerchant record);

    SysRoleMerchant selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRoleMerchant record);

    int updateByPrimaryKey(SysRoleMerchant record);
}