package com.alpha.module.sys.model;

import com.alpha.module.sys.entity.SysMenu;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class SysMenuDTO extends SysMenu {

    private int pageNo;

    private int pageSize;

    private String searchName;

    private Boolean check;

    private List<SysMenuDTO> children;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public SysMenu property() {
        SysMenu t = new SysMenu();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public static SysMenuDTO instance(SysMenu sysMenu) {
        if(sysMenu == null || sysMenu.getId() <= 0) {
            return null;
        }
        SysMenuDTO sysMenuDTO = new SysMenuDTO();
        BeanUtils.copyProperties(sysMenu, sysMenuDTO);
        return sysMenuDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public List<SysMenuDTO> getChildren() {
        return children;
    }

    public void setChildren(List<SysMenuDTO> children) {
        this.children = children;
    }
}
