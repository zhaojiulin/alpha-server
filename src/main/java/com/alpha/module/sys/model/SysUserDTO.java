package com.alpha.module.sys.model;

import com.alpha.module.sys.entity.SysUser;
import com.alpha.module.sys.entity.SysUserRole;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class SysUserDTO extends SysUser {

    private int pageNo;

    private int pageSize;

    private String searchName;

    private String merchantName;

    private List<SysUserRole> userRoles;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public SysUser property() {
        SysUser t = new SysUser();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public SysUserDTO instance(SysUser sysUser) {
        if(sysUser == null || sysUser.getId() <= 0) {
            return null;
        }
        SysUserDTO sysUserDTO = new SysUserDTO();
        BeanUtils.copyProperties(sysUser, sysUserDTO);
        return sysUserDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public List<SysUserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<SysUserRole> userRoles) {
        this.userRoles = userRoles;
    }
}
