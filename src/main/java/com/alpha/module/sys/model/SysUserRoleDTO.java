package com.alpha.module.sys.model;

import com.alpha.module.sys.entity.SysUserRole;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class SysUserRoleDTO extends SysUserRole {

    private int pageNo;

    private int pageSize;

    private String searchName;

    private List<Long> roleIds;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public SysUserRole property() {
        SysUserRole t = new SysUserRole();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public SysUserRoleDTO instance(SysUserRole sysUserRole) {
        if(sysUserRole == null || sysUserRole.getId() <= 0) {
            return null;
        }
        SysUserRoleDTO sysUserRoleDTO = new SysUserRoleDTO();
        BeanUtils.copyProperties(sysUserRole, sysUserRoleDTO);
        return sysUserRoleDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }
}
