package com.alpha.module.sys.model;

import com.alpha.module.sys.entity.SysConfig;
import org.springframework.beans.BeanUtils;

public class SysConfigDTO extends SysConfig {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public SysConfig property() {
        SysConfig t = new SysConfig();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public SysConfigDTO instance(SysConfig sysConfig) {
        if(sysConfig == null || sysConfig.getId() <= 0) {
            return null;
        }
        SysConfigDTO sysConfigDTO = new SysConfigDTO();
        BeanUtils.copyProperties(sysConfig, sysConfigDTO);
        return sysConfigDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
