package com.alpha.module.sys.model;

import com.alpha.module.sys.entity.SysRoleMerchant;
import org.springframework.beans.BeanUtils;

public class SysRoleMerchantDTO extends SysRoleMerchant {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public SysRoleMerchant property() {
        SysRoleMerchant t = new SysRoleMerchant();
        BeanUtils.copyProperties(this, t);
        return t;
    }

    /**
     * 持久对象转model
     * 2020/2/26
     */
    public SysRoleMerchantDTO instance(SysRoleMerchant sysRoleMerchant) {
        if(sysRoleMerchant == null || sysRoleMerchant.getId() <= 0) {
            return null;
        }
        SysRoleMerchantDTO sysRoleMerchantDTO = new SysRoleMerchantDTO();
        BeanUtils.copyProperties(sysRoleMerchant, sysRoleMerchantDTO);
        return sysRoleMerchantDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
