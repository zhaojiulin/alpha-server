package com.alpha.module.sys.model;

import com.alpha.module.sys.entity.SysMerchant;
import org.springframework.beans.BeanUtils;

public class SysMerchantDTO extends SysMerchant {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public SysMerchant property() {
        SysMerchant t = new SysMerchant();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public SysMerchantDTO instance(SysMerchant sysMerchant) {
        if(sysMerchant == null || sysMerchant.getId() <= 0) {
            return null;
        }
        SysMerchantDTO sysMerchantDTO = new SysMerchantDTO();
        BeanUtils.copyProperties(sysMerchant, sysMerchantDTO);
        return sysMerchantDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
