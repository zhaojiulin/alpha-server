package com.alpha.module.sys.model;

import com.alpha.module.sys.entity.SysRoleMenu;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class SysRoleMenuDTO extends SysRoleMenu {

    private int pageNo;

    private int pageSize;

    private String searchName;

    private Long userId;

    private Boolean check;

    private List<Long> menuIds;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public SysRoleMenu property() {
        SysRoleMenu t = new SysRoleMenu();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public SysRoleMenuDTO instance(SysRoleMenu sysRoleMenu) {
        if(sysRoleMenu == null || sysRoleMenu.getId() <= 0) {
            return null;
        }
        SysRoleMenuDTO sysRoleMenuDTO = new SysRoleMenuDTO();
        BeanUtils.copyProperties(sysRoleMenu, sysRoleMenuDTO);
        return sysRoleMenuDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public List<Long> getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(List<Long> menuIds) {
        this.menuIds = menuIds;
    }
}
