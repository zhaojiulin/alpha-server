package com.alpha.module.sys.model;

import com.alpha.module.sys.entity.SysRole;
import org.springframework.beans.BeanUtils;

public class SysRoleDTO extends SysRole {

    private int pageNo;

    private int pageSize;

    private String searchName;

    /**
     * vo转持久对象
     * 2020/2/26
     */
    public SysRole property() {
        SysRole t = new SysRole();
        BeanUtils.copyProperties(this,t);
        return t;
    }

    /**
     * 持久对象转vo
     * 2020/2/26
     */
    public SysRoleDTO instance(SysRole sysRole) {
        if(sysRole == null || sysRole.getId() <= 0) {
            return null;
        }
        SysRoleDTO sysRoleDTO = new SysRoleDTO();
        BeanUtils.copyProperties(sysRole, sysRoleDTO);
        return sysRoleDTO;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
