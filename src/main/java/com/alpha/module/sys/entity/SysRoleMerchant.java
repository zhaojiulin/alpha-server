package com.alpha.module.sys.entity;


import java.io.Serializable;

/**
 * 角色与商户对应关系
 * 
 * @author zhaojiulin
 * @date 2020-07-09 13:10:31
 */

public class SysRoleMerchant implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录id
	 */
	private Long id;
	/**
	 * 角色ID
	 */
	private Long roleId;
	/**
	 * 商户ID
	 */
	private Long merchantId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}
}
