package com.alpha.module.sys.entity;


import java.io.Serializable;

/**
 * 系统参数表：sys_config
 * 
 * @author zhaojiulin
 * @date 2020-07-09 13:10:31
 */

public class SysConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK
	 */
	private Long id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 标识
	 */
	private String nid;
	/**
	 * 值
	 */
	private String value;
	/**
	 * 域名
	 */
	private String domainName;
	/**
	 * 状态
	 */
	private Integer state;
	/**
	 * 分类
	 */
	private Integer type;
	/**
	 * 备注
	 */
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
