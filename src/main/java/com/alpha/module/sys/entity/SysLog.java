package com.alpha.module.sys.entity;


import java.io.Serializable;
import java.util.Date;


/**
 * 操作日志表
 * 
 * @author zhaojiulin
 * @date 2020-07-15 14:48:55
 */

public class SysLog implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * PK 
	 */
		private Long id;
	/**
	 * 操作名称 
	 */
	private String logName;
	/**
	 * 操作类型 
	 */
	private Integer logType;
	/**
	 * 内容 
	 */
	private String content;
	/**
	 * 管理员 
	 */
	private Long userIdCreate;
	/**
	 * 操作时间 
	 */
	private Date createTime;

	
	public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	public String getLogName(){
        return logName;
    }

    public void setLogName(String logName) {
        this.logName = logName;
    }
	
	public Integer getLogType(){
        return logType;
    }

    public void setLogType(Integer logType) {
        this.logType = logType;
    }
	
	public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
	
	public Long getUserIdCreate(){
        return userIdCreate;
    }

    public void setUserIdCreate(Long userIdCreate) {
        this.userIdCreate = userIdCreate;
    }
	
	public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
