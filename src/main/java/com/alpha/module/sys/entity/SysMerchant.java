package com.alpha.module.sys.entity;


import java.io.Serializable;
import java.util.Date;

/**
 * 商户管理
 * 
 * @author zhaojiulin
 * @date 2020-07-09 13:10:32
 */

public class SysMerchant implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 商户id
	 */
	private Long id;
	/**
	 * 上级商户ID，一级商户为0
	 */
	private Long parentId;
	/**
	 * 商户编码
	 */
	private String merchantCode;
	/**
	 * 商户名称
	 */
	private String merchantName;
	/**
	 * 商户名称(全称)
	 */
	private String fullName;
	/**
	 * 商户负责人
	 */
	private String director;
	/**
	 * 域名
	 */
	private String domainName;
	/**
	 * logo
	 */
	private String logo;
	/**
	 * 网站图标
	 */
	private String favicon;
	/**
	 * 联系邮箱
	 */
	private String email;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * 地址
	 */
	private String address;
	/**
	 * 可用标识  1：可用  0：不可用
	 */
	private Integer state;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date modifyTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getFavicon() {
		return favicon;
	}

	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
}
