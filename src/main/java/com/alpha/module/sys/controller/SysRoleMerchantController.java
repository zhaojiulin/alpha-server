package com.alpha.module.sys.controller;

import java.util.Map;


import com.alpha.module.sys.entity.SysRoleMerchant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alpha.module.sys.service.SysRoleMerchantService;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;



/**
 * 角色与商户对应关系
 *
 * @author zhaojiulin
 * @date 2020-07-08 17:38:44
 */
@RestController
@RequestMapping("alpha/sysrolemerchant")
public class SysRoleMerchantController {
    @Autowired
    private SysRoleMerchantService sysRoleMerchantService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestParam Map<String, Object> params) {
        PageUtils page = sysRoleMerchantService.queryPage(params);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		SysRoleMerchant sysRoleMerchant = sysRoleMerchantService.getById(id);

        return Res.ok().add("sysRoleMerchant", sysRoleMerchant);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody SysRoleMerchant sysRoleMerchant){
		sysRoleMerchantService.save(sysRoleMerchant);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody SysRoleMerchant sysRoleMerchant){
		sysRoleMerchantService.updateById(sysRoleMerchant);

        return Res.ok();
    }


}
