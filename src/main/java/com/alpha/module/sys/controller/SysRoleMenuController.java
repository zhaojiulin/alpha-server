package com.alpha.module.sys.controller;

import com.alpha.module.sys.entity.SysRoleMenu;
import com.alpha.module.sys.model.SysMenuDTO;
import com.alpha.module.sys.model.SysRoleMenuDTO;
import com.alpha.module.sys.service.SysRoleMenuService;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;



/**
 * 角色与菜单对应关系
 *
 * @author zhaojiulin
 * @date 2020-07-08 17:38:45
 */
@RestController
@RequestMapping("alpha/sysrolemenu")
public class SysRoleMenuController {
    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestParam Map<String, Object> params) {
        PageUtils page = sysRoleMenuService.queryPage(params);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		SysRoleMenu sysRoleMenu = sysRoleMenuService.getById(id);

        return Res.ok().add("sysRoleMenu", sysRoleMenu);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody SysRoleMenu sysRoleMenu){
		sysRoleMenuService.save(sysRoleMenu);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody SysRoleMenu sysRoleMenu){
		sysRoleMenuService.updateById(sysRoleMenu);

        return Res.ok();
    }

    @RequestMapping("/getRights")
    public Res rightsByUserId(long userId){
        List<SysMenuDTO>  menuRights = sysRoleMenuService.getUserRights(userId);
        return Res.ok().add("rights", menuRights);
    }

    @RequestMapping("/getRoleRights")
    public Res getRoleRights(long roleId){
        List<SysMenuDTO> roleRights = sysRoleMenuService.getRoleRights(roleId);
        return Res.ok().add("roleRights", roleRights);
    }

    @RequestMapping("/saveRoleRights")
    public Res saveRoleRights(@RequestBody SysRoleMenuDTO vo) {
        sysRoleMenuService.saveRoleRights(vo);
        return Res.ok();
    }

}
