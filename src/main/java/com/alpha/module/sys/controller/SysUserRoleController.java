package com.alpha.module.sys.controller;

import com.alpha.module.sys.entity.SysUserRole;
import com.alpha.module.sys.model.SysUserRoleDTO;
import com.alpha.module.sys.service.SysUserRoleService;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;



/**
 * 用户与角色对应关系
 *
 * @author zhaojiulin
 * @date 2020-07-08 17:38:45
 */
@RestController
@RequestMapping("alpha/sysuserrole")
public class SysUserRoleController {
    @Autowired
    private SysUserRoleService sysUserRoleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestParam Map<String, Object> params) {
        PageUtils page = sysUserRoleService.queryPage(params);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		SysUserRole sysUserRole = sysUserRoleService.getById(id);

        return Res.ok().add("sysUserRole", sysUserRole);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody SysUserRole sysUserRole){
		sysUserRoleService.save(sysUserRole);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody SysUserRole sysUserRole){
		sysUserRoleService.updateById(sysUserRole);

        return Res.ok();
    }

    @RequestMapping("/saveUserRoles")
    public Res saveRoleRights(@RequestBody SysUserRoleDTO vo) {
        sysUserRoleService.saveUserRoles(vo);
        return Res.ok();
    }


}
