package com.alpha.module.sys.controller;

import java.util.Map;


import com.alpha.module.sys.entity.SysRole;
import com.alpha.module.sys.model.SysRoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alpha.module.sys.service.SysRoleService;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;



/**
 * 系统角色
 *
 * @author zhaojiulin
 * @date 2020-07-08 17:38:45
 */
@RestController
@RequestMapping("alpha/sysrole")
public class SysRoleController {
    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestParam Map<String, Object> params) {
        PageUtils page = sysRoleService.queryPage(params);

        return Res.ok().add("page", page);
    }

    /**
     * join列表
     */
    @RequestMapping("/queryJoinList")
    public Res joinList(@RequestBody SysRoleDTO vo) {
        PageUtils page = sysRoleService.queryJoinList(vo);
        return Res.ok().add("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		SysRole sysRole = sysRoleService.getById(id);

        return Res.ok().add("sysRole", sysRole);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody SysRole sysRole){
		sysRoleService.save(sysRole);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody SysRole sysRole){
		sysRoleService.updateById(sysRole);

        return Res.ok();
    }


}
