package com.alpha.module.sys.controller;


import com.alpha.module.sys.entity.SysUser;
import com.alpha.module.sys.model.SysUserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alpha.module.sys.service.SysUserService;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;



/**
 * 系统用户
 *
 * @author zhaojiulin
 * @date 2020-07-08 17:38:45
 */
@RestController
@RequestMapping("alpha/sysuser")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody SysUserDTO vo) {
        PageUtils page = sysUserService.queryPage(vo);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		SysUser sysUser = sysUserService.getById(id);

        return Res.ok().add("sysUser", sysUser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody SysUser sysUser){
		sysUserService.save(sysUser);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody SysUser sysUser){
		sysUserService.updateById(sysUser);

        return Res.ok();
    }

    /**
     * 登录
     */
    @RequestMapping("/login")
    public Res login(@RequestBody SysUserDTO vo){
        SysUser sysUser = sysUserService.login(vo);
        return Res.ok().add("sysUser", sysUser);
    }
}
