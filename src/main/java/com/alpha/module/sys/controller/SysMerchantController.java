package com.alpha.module.sys.controller;

import com.alpha.module.sys.entity.SysMerchant;
import com.alpha.module.sys.model.SysMerchantDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.alpha.module.sys.service.SysMerchantService;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;



/**
 * 商户管理
 *
 * @author zhaojiulin
 * @date 2020-07-08 17:38:45
 */
@RestController
@RequestMapping("alpha/sysmerchant")
public class SysMerchantController {
    @Autowired
    private SysMerchantService sysMerchantService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody SysMerchantDTO vo) {
        PageUtils page = sysMerchantService.queryPage(vo);

        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		SysMerchant sysMerchant = sysMerchantService.getById(id);

        return Res.ok().add("sysMerchant", sysMerchant);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody SysMerchant sysMerchant){
		sysMerchantService.save(sysMerchant);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody SysMerchant sysMerchant){
		sysMerchantService.updateById(sysMerchant);
        return Res.ok();
    }
    /**
     * 根据域名获取商户信息
     */
    @RequestMapping("/getMerchantDomainName")
    public Res update(@RequestBody SysMerchantDTO sysMerchantDTO){
        SysMerchant sysMerchant = sysMerchantService.getByDomainName(sysMerchantDTO.getDomainName());
        return Res.ok().add("sysMerchant", sysMerchant);
    }


}
