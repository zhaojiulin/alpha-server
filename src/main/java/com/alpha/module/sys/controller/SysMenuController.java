package com.alpha.module.sys.controller;


import com.alpha.module.sys.entity.SysMenu;
import com.alpha.module.sys.model.SysMenuDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alpha.module.sys.service.SysMenuService;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;



/**
 * 菜单管理
 *
 * @author zhaojiulin
 * @date 2020-07-08 17:38:45
 */
@RestController
@RequestMapping("alpha/sysmenu")
public class SysMenuController {
    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody SysMenuDTO vo) {
        PageUtils page = sysMenuService.queryPage(vo);
        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		SysMenu sysMenu = sysMenuService.getById(id);

        return Res.ok().add("sysMenu", sysMenu);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody SysMenu sysMenu){
		sysMenuService.save(sysMenu);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody SysMenu sysMenu){
		sysMenuService.updateById(sysMenu);

        return Res.ok();
    }


}
