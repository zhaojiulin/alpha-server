package com.alpha.module.sys.controller;


import com.alpha.module.sys.model.SysConfigDTO;
import com.alpha.module.sys.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alpha.module.sys.entity.SysConfig;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.Res;

/**
 * 系统参数表：sys_config
 *
 * @author zhaojiulin
 * @date 2020-07-09 13:10:31
 */
@RestController
@RequestMapping("alpha/sysconfig")
public class SysConfigController {
    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Res list(@RequestBody SysConfigDTO vo) {
        PageUtils page = sysConfigService.queryPage(vo);
        return Res.ok().add("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Res info(@PathVariable("id") Long id){
		SysConfig sysConfig = sysConfigService.getById(id);

        return Res.ok().add("sysConfig", sysConfig);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Res save(@RequestBody SysConfig sysConfig){
		sysConfigService.save(sysConfig);

        return Res.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Res update(@RequestBody SysConfig sysConfig){
		sysConfigService.updateById(sysConfig);

        return Res.ok();
    }


}
