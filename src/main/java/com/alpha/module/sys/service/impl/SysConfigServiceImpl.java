package com.alpha.module.sys.service.impl;

import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.alpha.components.GlobalConfigMap;
import com.alpha.module.sys.dao.SysConfigMapper;
import com.alpha.module.sys.entity.SysConfig;
import com.alpha.module.sys.model.SysConfigDTO;
import com.alpha.module.sys.service.SysConfigService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("sysConfigService")
public class SysConfigServiceImpl implements SysConfigService {

    private final SysConfigMapper sysConfigMapper;

    public SysConfigServiceImpl(SysConfigMapper sysConfigMapper) {
        this.sysConfigMapper = sysConfigMapper;
    }

    @Override
    public PageUtils queryPage(SysConfigDTO vo) {
        PageHelper.startPage(vo.getPageNo(), vo.getPageSize());
        List<SysConfig> list = this.sysConfigMapper.queryList(vo);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(SysConfig sysConfig) {
        sysConfigMapper.insertSelective(sysConfig);
    }

    @Override
    public SysConfig getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数【ID】错误");
        }
        return sysConfigMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(SysConfig sysConfig) {
        sysConfigMapper.updateByPrimaryKeySelective(sysConfig);
        GlobalConfigMap.updateKeyConfig(sysConfig.getNid(), sysConfig.getValue());
    }

    public List<SysConfig> cacheAllConfig() {
        return this.sysConfigMapper.queryAll();
    }

}