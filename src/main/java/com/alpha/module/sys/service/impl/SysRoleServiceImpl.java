package com.alpha.module.sys.service.impl;

import com.alpha.module.sys.entity.SysRole;
import com.alpha.module.sys.model.SysRoleDTO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.BusinessException;

import com.alpha.module.sys.dao.SysRoleMapper;
import com.alpha.module.sys.service.SysRoleService;


@Service("sysRoleService")
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        List<SysRole> list = null;
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public PageUtils queryJoinList(SysRoleDTO vo) {
        PageHelper.startPage(vo.getPageNo(),vo.getPageSize());
        List<HashMap> list = this.sysRoleMapper.queryJoinList(vo);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(SysRole sysRole) {
        sysRole.setCreateTime(new Date());
            sysRoleMapper.insertSelective(sysRole);
    }

    @Override
    public SysRole getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return sysRoleMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(SysRole sysRole) {
        sysRole.setModifyTime(new Date());
            sysRoleMapper.updateByPrimaryKeySelective(sysRole);
    }


}