package com.alpha.module.sys.service;

import com.alpha.module.sys.entity.SysRoleMerchant;
import com.alpha.common.utils.PageUtils;

import java.util.Map;

/**
 * 角色与商户对应关系
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:10
 */
public interface SysRoleMerchantService {

    /**
    * 获取列表
    * @param params
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(Map<String, Object> params);

    /**
    * 保存
    */
    void save(SysRoleMerchant sysRoleMerchant);

    /**
    * 根据id获取实例
     * @param id id
    */
    SysRoleMerchant getById(Long id);


    /**
    * 更新
     */
    void updateById(SysRoleMerchant sysRoleMerchant);

}

