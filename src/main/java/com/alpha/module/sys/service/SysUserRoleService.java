package com.alpha.module.sys.service;

import com.alpha.module.sys.entity.SysUserRole;
import com.alpha.module.sys.model.SysUserRoleDTO;
import com.alpha.common.utils.PageUtils;

import java.util.Map;

/**
 * 用户与角色对应关系
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:11
 */
public interface SysUserRoleService {

    /**
    * 获取列表
    * @param params
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(Map<String, Object> params);

    /**
    * 保存
    */
    void save(SysUserRole sysUserRole);

    /**
    * 根据id获取实例
     * @param id id
    */
    SysUserRole getById(Long id);


    /**
    * 更新
     */
    void updateById(SysUserRole sysUserRole);

    void saveUserRoles(SysUserRoleDTO vo);

}

