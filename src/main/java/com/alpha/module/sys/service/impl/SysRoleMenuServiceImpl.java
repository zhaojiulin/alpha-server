package com.alpha.module.sys.service.impl;

import com.alpha.module.sys.dao.SysMenuMapper;
import com.alpha.module.sys.dao.SysRoleMenuMapper;
import com.alpha.module.sys.dao.SysUserRoleMapper;
import com.alpha.module.sys.entity.SysMenu;
import com.alpha.module.sys.entity.SysRoleMenu;
import com.alpha.module.sys.entity.SysUserRole;
import com.alpha.module.sys.model.SysMenuDTO;
import com.alpha.module.sys.model.SysRoleMenuDTO;
import com.alpha.module.sys.service.SysRoleMenuService;
import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl implements SysRoleMenuService {

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Autowired
    private SysUserRoleMapper  sysUserRoleMapper;

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        List<SysRoleMenu> list = null;
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(SysRoleMenu sysRoleMenu) {
            sysRoleMenuMapper.insertSelective(sysRoleMenu);
    }

    @Override
    public SysRoleMenu getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return sysRoleMenuMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(SysRoleMenu sysRoleMenu) {
            sysRoleMenuMapper.updateByPrimaryKeySelective(sysRoleMenu);
    }

    @Override
    public List<SysMenuDTO> getUserRights(long userId) {
        return this.getRights(userId, 0L);
    }

    @Override
    public List<SysMenuDTO> getRoleRights(long roleId) {
        return this.getRights(0L, roleId);
    }

    private List<SysMenuDTO> getRights(long userId, long roleId) {
        List<SysMenu> allMenu = this.sysMenuMapper.selectAllMenu();
        List<SysMenuDTO> menuVOS = new ArrayList<>();
        for (SysMenu menu : allMenu) {
            SysMenuDTO model = SysMenuDTO.instance(menu);
            menuVOS.add(model);
        }
        List<Long> roleIds = new ArrayList<>();
        if (userId > 0) {
            List<SysUserRole> userRoles = this.sysUserRoleMapper.selectRolesByUserId(userId);
            for (SysUserRole role : userRoles) {
                roleIds.add(role.getRoleId());
            }
        } else {
            roleIds.add(roleId);
        }
        HashMap<String, SysRoleMenu> map = new HashMap<>();
        List<SysRoleMenu> userRights = this.sysRoleMenuMapper.selectRights(roleIds);
        for (SysRoleMenu r : userRights) {
            map.put(r.getMenuId().toString(), r);
        }
        for (SysMenuDTO smv : menuVOS) {
            smv.setCheck(map.containsKey(smv.getId().toString()));
        }
        return F(menuVOS, 0L);
    }

    private List<SysMenuDTO> F(List<SysMenuDTO> list, long parentId) {
        List<SysMenuDTO> childList = this.getChildList(list, parentId);
        if (childList.size() > 0) {
            for (SysMenuDTO sysMenuDTO : childList) {
                sysMenuDTO.setChildren(F(list, sysMenuDTO.getId()));
            }
            return childList;
        } else {
            return null;
        }
    }

    private List<SysMenuDTO> getChildList(List<SysMenuDTO> list, long parentId) {
        List<SysMenuDTO> childList = new ArrayList<>();
        for (SysMenuDTO r : list) {
            if (r.getParentId() == parentId)
                childList.add(r);
        }
        return childList;
    }

    @Override
    public void saveRoleRights(SysRoleMenuDTO vo) {
        if (vo.getRoleId() == null && vo.getRoleId() <= 0) {
            throw new BusinessException("角色参数错误");
        }
        if (vo.getMenuIds().size() <=0) {
            throw new BusinessException("权限参数错误");
        }
        sysRoleMenuMapper.deleteByRoleId(vo.getRoleId());
        SysRoleMenu sys = new SysRoleMenu();
        for (Long menuId : vo.getMenuIds()) {
            sys.setRoleId(vo.getRoleId());
            sys.setMenuId(menuId);
            sysRoleMenuMapper.insert(sys);
        }
    }
}