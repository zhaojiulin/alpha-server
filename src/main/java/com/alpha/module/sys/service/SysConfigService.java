package com.alpha.module.sys.service;

import com.alpha.module.sys.model.SysConfigDTO;
import com.alpha.common.utils.PageUtils;
import com.alpha.module.sys.entity.SysConfig;

import java.util.List;

/**
 * 系统参数表：sys_config
 * dao
 * @author zhaojiulin
 * @date 2020-07-09 13:10:31
 */
public interface SysConfigService {

    /**
     * 获取列表
     * @param vo
     * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
     */
    PageUtils queryPage(SysConfigDTO vo);

    /**
     * 保存
     */
    void save(SysConfig sysConfig);

    /**
     * 根据id获取实例
     * @param id id
     */
    SysConfig getById(Long id);


    /**
     * 更新
     */
    void updateById(SysConfig sysConfig);


    List<SysConfig> cacheAllConfig();

}

