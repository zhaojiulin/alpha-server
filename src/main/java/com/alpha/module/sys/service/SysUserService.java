package com.alpha.module.sys.service;

import com.alpha.module.sys.entity.SysUser;
import com.alpha.module.sys.model.SysUserDTO;
import com.alpha.common.utils.PageUtils;

/**
 * 系统用户
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:11
 */
public interface SysUserService {

    /**
    * 获取列表
    * @param vo
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(SysUserDTO vo);

    /**
    * 保存
    */
    void save(SysUser sysUser);

    /**
    * 根据id获取实例
     * @param id id
    */
    SysUser getById(Long id);


    /**
    * 更新
     */
    void updateById(SysUser sysUser);

    /**
     * 登录
      * @param sysUser 数据
     * @return 用户
     */
    SysUser login(SysUserDTO vo);

}

