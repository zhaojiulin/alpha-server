package com.alpha.module.sys.service;


import com.alpha.module.sys.entity.SysLog;
import com.alpha.module.sys.model.SysLogDTO;
import com.alpha.common.utils.PageUtils;

/**
 * 操作日志表
 * dao
 * @author zhaojiulin
 * @date 2020-07-15 15:03:37
 */
public interface SysLogService {

    /**
    * 获取列表
    * @param dto
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(SysLogDTO dto);

    /**
    * 保存
    */
    void save(SysLog sysLog);

    /**
    * 根据id获取实例
     * @param id id
    */
    SysLog getById(Long id);


    /**
    * 更新
     */
    void updateById(SysLog sysLog);

}

