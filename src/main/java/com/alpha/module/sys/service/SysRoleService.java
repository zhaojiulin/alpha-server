package com.alpha.module.sys.service;

import com.alpha.module.sys.entity.SysRole;
import com.alpha.module.sys.model.SysRoleDTO;
import com.alpha.common.utils.PageUtils;

import java.util.Map;

/**
 * 系统角色
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:11
 */
public interface SysRoleService {

    /**
    * 获取列表
    * @param params
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryJoinList(SysRoleDTO vo);

    /**
    * 保存
    */
    void save(SysRole sysRole);

    /**
    * 根据id获取实例
     * @param id id
    */
    SysRole getById(Long id);


    /**
    * 更新
     */
    void updateById(SysRole sysRole);

}

