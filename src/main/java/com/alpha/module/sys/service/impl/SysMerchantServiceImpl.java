package com.alpha.module.sys.service.impl;

import com.alpha.module.sys.entity.SysMerchant;
import com.alpha.module.sys.model.SysMerchantDTO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.BusinessException;

import com.alpha.module.sys.dao.SysMerchantMapper;
import com.alpha.module.sys.service.SysMerchantService;


@Service("sysMerchantService")
public class SysMerchantServiceImpl implements SysMerchantService {

    @Autowired
    private SysMerchantMapper sysMerchantMapper;

    @Override
    public PageUtils queryPage(SysMerchantDTO vo) {
        PageHelper.startPage(vo.getPageNo(), vo.getPageSize());
        List<SysMerchant> list = this.sysMerchantMapper.queryList(vo);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(SysMerchant sysMerchant) {
        sysMerchant.setCreateTime(new Date());
        sysMerchantMapper.insertSelective(sysMerchant);
    }

    @Override
    public SysMerchant getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return sysMerchantMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(SysMerchant sysMerchant) {
        sysMerchant.setModifyTime(new Date());
        sysMerchantMapper.updateByPrimaryKeySelective(sysMerchant);
    }

    @Override
    public SysMerchant getByDomainName(String domainName) {
       return sysMerchantMapper.selectByDomainName(domainName);
    }


}