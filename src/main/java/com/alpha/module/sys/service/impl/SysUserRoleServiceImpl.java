package com.alpha.module.sys.service.impl;

import com.alpha.module.sys.dao.SysUserRoleMapper;
import com.alpha.module.sys.entity.SysUserRole;
import com.alpha.module.sys.model.SysUserRoleDTO;
import com.alpha.module.sys.service.SysUserRoleService;
import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("sysUserRoleService")
public class SysUserRoleServiceImpl implements SysUserRoleService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        List<SysUserRole> list = null;
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(SysUserRole sysUserRole) {
            sysUserRoleMapper.insertSelective(sysUserRole);
    }

    @Override
    public SysUserRole getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return sysUserRoleMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(SysUserRole sysUserRole) {
            sysUserRoleMapper.updateByPrimaryKeySelective(sysUserRole);
    }

    @Override
    public void saveUserRoles(SysUserRoleDTO vo) {
        if (vo.getUserId() == null && vo.getUserId() <= 0) {
            throw new BusinessException("参数错误");
        }
        if (vo.getRoleIds().size() <=0) {
            throw new BusinessException("参数错误");
        }
        sysUserRoleMapper.deleteByUserId(vo.getUserId());
        SysUserRole sys = new SysUserRole();
        for (Long roleId : vo.getRoleIds()) {
            sys.setUserId(vo.getUserId());
            sys.setRoleId(roleId);
            sysUserRoleMapper.insert(sys);
        }
    }

}