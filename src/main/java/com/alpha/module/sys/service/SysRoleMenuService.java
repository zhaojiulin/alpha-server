package com.alpha.module.sys.service;

import com.alpha.module.sys.entity.SysRoleMenu;
import com.alpha.module.sys.model.SysMenuDTO;
import com.alpha.module.sys.model.SysRoleMenuDTO;
import com.alpha.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 角色与菜单对应关系
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:11
 */
public interface SysRoleMenuService {

    /**
    * 获取列表
    * @param params
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(Map<String, Object> params);

    /**
    * 保存
    */
    void save(SysRoleMenu sysRoleMenu);

    /**
    * 根据id获取实例
     * @param id id
    */
    SysRoleMenu getById(Long id);


    /**
    * 更新
     */
    void updateById(SysRoleMenu sysRoleMenu);

    List<SysMenuDTO> getUserRights(long userId);

    List<SysMenuDTO> getRoleRights(long roleId);

    void saveRoleRights(SysRoleMenuDTO vo);

}

