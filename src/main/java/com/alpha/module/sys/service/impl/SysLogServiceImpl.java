package com.alpha.module.sys.service.impl;

import com.alpha.module.sys.dao.SysLogMapper;
import com.alpha.module.sys.entity.SysLog;
import com.alpha.module.sys.model.SysLogDTO;
import com.alpha.module.sys.service.SysLogService;
import com.alpha.common.utils.BusinessException;
import com.alpha.common.utils.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("sysLogService")
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogMapper sysLogMapper;

    @Override
    public PageUtils queryPage(SysLogDTO dto) {
        PageHelper.startPage(dto.getPageNo(), dto.getPageSize());
        List<SysLog> list = this.sysLogMapper.queryList(dto);
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(SysLog sysLog) {
            sysLogMapper.insertSelective(sysLog);
    }

    @Override
    public SysLog getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return sysLogMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(SysLog sysLog) {
            sysLogMapper.updateByPrimaryKeySelective(sysLog);
    }


}