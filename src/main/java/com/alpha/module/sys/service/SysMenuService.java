package com.alpha.module.sys.service;

import com.alpha.module.sys.entity.SysMenu;
import com.alpha.module.sys.model.SysMenuDTO;
import com.alpha.common.utils.PageUtils;

/**
 * 菜单管理
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:11
 */
public interface SysMenuService {

    /**
    * 获取列表
    * @param params
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(SysMenuDTO vo);

    /**
    * 保存
    */
    void save(SysMenu sysMenu);

    /**
    * 根据id获取实例
     * @param id id
    */
    SysMenu getById(Long id);


    /**
    * 更新
     */
    void updateById(SysMenu sysMenu);

}

