package com.alpha.module.sys.service;

import com.alpha.module.sys.entity.SysMerchant;
import com.alpha.module.sys.model.SysMerchantDTO;
import com.alpha.common.utils.PageUtils;

/**
 * 商户管理
 * dao
 * @author zhaojiulin
 * @date 2020-07-08 17:41:11
 */
public interface SysMerchantService {

    /**
    * 获取列表
    * @param vo
    * @return  {currPage:1,pageSize:10,totalCount:0,totalPage:1,list}
    */
    PageUtils queryPage(SysMerchantDTO vo);

    /**
    * 保存
    */
    void save(SysMerchant sysMerchant);

    /**
    * 根据id获取实例
     * @param id id
    */
    SysMerchant getById(Long id);


    /**
    * 更新
     */
    void updateById(SysMerchant sysMerchant);

    /**
     * 根据域名获取商户信息
     * @param domainName 域名
     * @return 商户
     */
    SysMerchant getByDomainName(String domainName);

}

