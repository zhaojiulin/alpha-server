package com.alpha.module.sys.service.impl;

import com.alpha.module.sys.entity.SysRoleMerchant;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import com.alpha.common.utils.PageUtils;
import com.alpha.common.utils.BusinessException;

import com.alpha.module.sys.dao.SysRoleMerchantMapper;
import com.alpha.module.sys.service.SysRoleMerchantService;


@Service("sysRoleMerchantService")
public class SysRoleMerchantServiceImpl implements SysRoleMerchantService {

    @Autowired
    private SysRoleMerchantMapper sysRoleMerchantMapper;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        List<SysRoleMerchant> list = null;
        return new PageUtils(new PageInfo<>(list));
    }

    @Override
    public void save(SysRoleMerchant sysRoleMerchant) {
            sysRoleMerchantMapper.insertSelective(sysRoleMerchant);
    }

    @Override
    public SysRoleMerchant getById(Long id) {
        if (id <= 0) {
            throw new BusinessException("参数错误");
        }
        return sysRoleMerchantMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateById(SysRoleMerchant sysRoleMerchant) {
            sysRoleMerchantMapper.updateByPrimaryKeySelective(sysRoleMerchant);
    }


}