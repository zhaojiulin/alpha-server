
DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK ',
  `user_id` int(11) DEFAULT '0' COMMENT '用户 ',
  `total_amount` decimal(20,2) DEFAULT '0.00' COMMENT '账户总金额 ',
  `balance` decimal(20,2) DEFAULT '0.00' COMMENT '可用余额 ',
  `integral` int(11) DEFAULT '0' COMMENT '可用积分 ',
  `version` varchar(128) DEFAULT NULL COMMENT '版本控制 ',
  `merchant_id` int(10) DEFAULT NULL COMMENT '所属商户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='账户资产表：account';


INSERT INTO `account` (`id`, `user_id`, `total_amount`, `balance`, `integral`, `version`, `merchant_id`)
VALUES
	(1,2,0.00,0.00,0,'7a364d84fb4c04f8e21009f5fcf1b477',NULL),
	(2,3,0.00,0.00,0,'21ce314667351414922eb1ff4b6bd489',NULL),
	(3,4,0.00,0.00,0,'acf38aa4d6102859c48476cfdeff560f',NULL),
	(4,5,0.00,0.00,0,'5e982e96bd6116da92ba2ce52e23abae',NULL);



DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `nid` varchar(68) DEFAULT NULL COMMENT '所属栏目',
  `title` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '标题',
  `introduction` varchar(512) CHARACTER SET utf8 DEFAULT NULL COMMENT '简介',
  `content` text CHARACTER SET utf8 COMMENT '内容',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `is_hot` int(11) DEFAULT '0' COMMENT '热文章',
  `clicks` int(11) DEFAULT '0' COMMENT '点击量',
  `pic_path` varchar(512) CHARACTER SET utf8 DEFAULT NULL COMMENT '图片地址',
  `url` varchar(512) CHARACTER SET utf8 DEFAULT NULL COMMENT '链接',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `merchant_id` bigint(11) DEFAULT '1' COMMENT '商户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COMMENT='article';



INSERT INTO `article` (`id`, `nid`, `title`, `introduction`, `content`, `state`, `sort`, `is_hot`, `clicks`, `pic_path`, `url`, `update_time`, `merchant_id`)
VALUES
	(1,'1','注册&注销需要本人到场吗？','注册？','注册&注销需要本人到场吗？v注册&注销需要本人到场吗？注册&注销需要本人到场吗？注册&注销需要本人到场吗？',1,1,1,2,'string','string','2020-05-19 09:41:51',1),
	(2,'1','注册&注销需要本人到场吗？','注册？','注册&注销需要本人到场吗？v注册&注销需要本人到场吗？注册&注销需要本人到场吗？注册&注销需要本人到场吗？',1,1,1,2,'string','string','2020-05-19 09:41:51',1),
	(3,'2','BATJ面试题汇总资源？','注册？','注册&注销需要本人到场吗？v注册&注销需要本人到场吗？注册&注销需要本人到场吗？注册&注销需要本人到场吗？',1,1,1,2,'string','string','2020-05-19 09:41:51',1),
	(4,'atc-faq','BATJ面试题汇总资源？','注册？','注册&注销需要本人到场吗？v注册&注销需要本人到场吗？注册&注销需要本人到场吗？注册&注销需要本人到场吗？',1,1,1,2,'string','string','2020-05-19 09:41:51',1),
	(5,'atc-faq','LoppBack4入门？','注册？','lb4 model，lb4 dicover,lb4 datasource？',1,1,1,2,'string','string','2020-05-19 09:41:51',1),
	(6,'atc-faq','LoppBack4进阶？','注册？','lb4 model，lb4 dicover,lb4 datasource？',1,1,1,2,'string','string','2020-05-19 09:41:51',1),
	(7,'atc-notice','今天天气真的不错哦','string','string',1,0,0,0,'string','string','2020-05-20 02:28:10',1),
	(8,'string','string','string','string',0,0,0,0,'string','string','2020-06-01 03:18:39',0),
	(9,'今天天气真的不错哦','params.introduction.toString()','act-banner','params.url.toString()',1,0,0,0,'/upload/article_511591431588452.png','params.url.toString()','2020-06-08 01:34:54',1),
	(10,'atc-banner','炎炎夏日','堡垒之夜 你最甜','苏打粉撒',1,NULL,NULL,NULL,'https://static-1302432339.cos.ap-shanghai.myqcloud.com/banner/vc-upload-1597125719836-6.jpeg',NULL,NULL,1),
	(11,'atc-banner','绒花','大热天的','是的',1,NULL,NULL,NULL,'https://shqskjyxgs-1302432339.cos.ap-shanghai.myqcloud.com/article_img/vc-upload-1597650079959-3.jpeg','/ib',NULL,1);


DROP TABLE IF EXISTS `sys_config`;

CREATE TABLE `sys_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `nid` varchar(64) DEFAULT NULL COMMENT '标识',
  `value` varchar(256) DEFAULT NULL COMMENT '值',
  `domain_name` varchar(128) DEFAULT NULL COMMENT '域名',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `type` int(11) DEFAULT NULL COMMENT '分类',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='系统参数表：sys_config';



INSERT INTO `sys_config` (`id`, `name`, `nid`, `value`, `domain_name`, `state`, `type`, `remark`)
VALUES
	(1,'系统状态','sys_state','dev','www.tanhaogong.com',1,1,'无'),
	(2,'九重天科技','obove_sky','{\"appId\":\"1001787\",\"url\":\"https://zmonapi.zmxy.com.cn/openapi.do\"}','www.aboveSky.com',-1,3,'商户'),
	(11,'测试','test','是的发送','192.168.31.212',1,1,'ddd');


DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `component` varchar(500) DEFAULT NULL COMMENT '可为 页面关键字：如页面组件；操作关键字：如系统用户页面的添加：sys:user:add',
  `type` int(4) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `state` int(4) DEFAULT NULL COMMENT '状态   1：正常   2：禁用',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT '0' COMMENT '排序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COMMENT='菜单管理';


INSERT INTO `sys_menu` (`parent_id`, `name`, `url`, `component`, `type`, `state`, `icon`, `order_num`, `create_time`, `modify_time`)
VALUES
	(0,'系统管理',NULL,NULL,1,1,'setting',1,'2020-08-09 22:55:15','2020-08-02 22:42:14'),
	(1,'角色管理','/system/role-list','RoleList',1,1,NULL,1,'2020-08-09 22:55:15','2020-08-20 10:00:12'),
	(1,'菜单权限','/system/menu-list','MenuTreeList',1,1,NULL,1,'2020-08-09 22:55:15','2020-08-20 10:00:12'),
	(1,'系统参数','/system/system-config','SystemConfig',1,1,NULL,1,'2020-08-09 22:55:15','2020-08-20 10:00:12'),
	(1,'商户管理','/system/merchant-list',NULL,1,1,'robot',0,'2020-08-04 21:28:00',NULL),
	(0,'内容管理',NULL,'RouteView',1,1,'project',1,'2020-08-09 22:55:15','2020-08-10 00:56:36'),
	(5,'文章列表','/content/article-list','ArticleList',1,1,NULL,1,'2020-08-09 22:55:15','2020-08-20 10:00:12'),
	(6,'添加',NULL,'add',2,1,NULL,1,'2020-08-09 22:55:15','2020-08-09 22:55:15'),
	(1,'管理员','/system/sysUser-list','SysUserList',1,1,NULL,1,'2020-08-09 22:55:15','2020-08-09 22:55:15'),
	(0,'用户管理',NULL,NULL,1,1,'user',0,'2020-08-02 21:06:24',NULL),
	(10,'用户记录','/customer/list',NULL,1,1,NULL,0,'2020-08-02 21:07:26','2020-08-03 20:47:57'),


DROP TABLE IF EXISTS `sys_merchant`;

CREATE TABLE `sys_merchant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商户id',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级商户ID，一级商户为0',
  `merchant_code` varchar(100) DEFAULT NULL COMMENT '商户编码',
  `merchant_name` varchar(100) DEFAULT NULL COMMENT '商户名称',
  `full_name` varchar(100) DEFAULT NULL COMMENT '商户名称(全称)',
  `director` varchar(100) DEFAULT NULL COMMENT '商户负责人',
  `domain_name` varchar(100) DEFAULT NULL COMMENT '域名',
  `logo` varchar(255) DEFAULT NULL COMMENT 'logo',
  `favicon` varchar(255) DEFAULT NULL COMMENT '网站图标',
  `email` varchar(100) DEFAULT NULL COMMENT '联系邮箱',
  `phone` varchar(100) DEFAULT NULL COMMENT '联系电话',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `state` int(4) DEFAULT '1' COMMENT '可用标识  1：可用  0：不可用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='商户管理';



INSERT INTO `sys_merchant` (`id`, `parent_id`, `merchant_code`, `merchant_name`, `full_name`, `director`, `domain_name`, `logo`, `favicon`, `email`, `phone`, `address`, `state`, `create_time`, `modify_time`)
VALUES
	(1,0,'shqskjyxgs','弹好工','神乎其神科技有限公司','海信','zhaojiulin.top',NULL,NULL,'jiulinz@163.com','15057245204','浙江省航海走势',1,'2020-05-08 22:06:58','2020-08-19 20:43:46'),
	(2,0,'wlkjcyxgs','科技城','未来科技城有限公司','张三','192.168.31.212',NULL,NULL,'948526959@qq.com','15057245204','浙江省航海走势',1,'2020-08-04 22:13:54','2020-08-16 22:39:13'),
	(3,0,'hzmmcw','喵呜喵','杭州喵喵宠物','赵九林','miao.zhaojiulin.top','https://shqskjyxgs-1302432339.cos.ap-shanghai.myqcloud.com/logo_img/vc-upload-1597717394580-3.jpeg','https://shqskjyxgs-1302432339.cos.ap-shanghai.myqcloud.com/favicon_img/vc-upload-1597717394580-5.jpeg','jiulinz@163.com','17757140334','杭州市西湖区支付宝大楼',1,'2020-08-17 21:25:40','2020-08-17 21:26:14');


DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `merchant_id` bigint(255) DEFAULT NULL COMMENT '所属商户',
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `state` int(4) DEFAULT NULL COMMENT '状态',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `modify_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='系统角色';


INSERT INTO `sys_role` (`id`, `merchant_id`, `role_name`, `state`, `remark`, `user_id_create`, `create_time`, `modify_time`)
VALUES
	(1,1,'超级管理员',1,'【系统内置】',2,'2017-08-12 00:43:52','2017-11-21 10:19:08'),
	(2,1,'商户管理员',1,'【商户】',1,'2020-05-08 22:31:29','2020-08-01 02:52:35'),
	(3,1,'园区管理员',1,'【园区】',NULL,'2020-07-30 22:22:34','2020-07-30 22:23:15'),
	(4,1,'运营人员',1,NULL,NULL,'2020-08-15 04:59:29',NULL);


DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';


INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`)
VALUES
	(14,3,9),
	(15,3,10),
	(160,1,1),
	(161,1,2),
	(162,1,3),
	(163,1,4),
	(164,1,8),
	(165,1,18),
	(166,1,5),
	(167,1,6),
	(168,1,9),
	(169,1,10),
	(170,1,13),
	(171,1,19),
	(172,1,14),
	(173,1,15),
	(174,1,16),
	(175,1,17),
	(176,1,20),
	(177,1,21),
	(178,1,22),
	(179,1,23),
	(180,2,1),
	(181,2,8),
	(182,2,5),
	(183,2,6),
	(184,2,9),
	(185,2,10),
	(186,2,14),
	(187,2,15),
	(188,2,16),
	(189,2,17),
	(190,2,20),
	(191,2,21),
	(192,2,22),
	(193,2,23);


DROP TABLE IF EXISTS `sys_role_merchant`;

CREATE TABLE `sys_role_merchant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `merchant_id` bigint(20) DEFAULT NULL COMMENT '商户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与商户对应关系';



DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `nickname` varchar(50) DEFAULT NULL COMMENT '姓名(昵称)',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '登录用户名',
  `credential` varchar(50) NOT NULL COMMENT '登录凭证',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `state` tinyint(4) DEFAULT '1' COMMENT '状态 0:禁用，1:正常',
  `type` int(4) DEFAULT '1' COMMENT '类型',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '上级',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `merchant_id` bigint(20) NOT NULL COMMENT '所属商户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='系统用户';


INSERT INTO `sys_user` (`id`, `nickname`, `username`, `credential`, `email`, `mobile`, `state`, `type`, `remark`, `user_id_create`, `create_time`, `modify_time`, `merchant_id`)
VALUES
	(1,'超级管理员','admin','bdee6b899235f80cf6063da3d818ebe5','100000@qq.com','17752859653',1,1,NULL,1,'2017-08-15 21:40:39','2020-05-11 21:55:19',1),
	(2,'园区管理员','yuanqu','e174fe5252576b6a2539af8e49ad658d','jiulinz@163.com','15057245204',1,2,'sadf',NULL,'2020-07-31 02:16:19','2020-08-01 01:13:37',1);



DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';


INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`)
VALUES
	(11,1,1),
	(12,2,2);


DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK ',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名 ',
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机号 ',
  `email` varchar(32) DEFAULT NULL COMMENT '邮箱 ',
  `avatar` text COMMENT '头像 ',
  `state` int(11) DEFAULT NULL COMMENT '状态 ',
  `merchant_id` bigint(11) DEFAULT '1' COMMENT '商户id',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间 ',
  `invite_code` bigint(11) DEFAULT NULL COMMENT '邀请人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='用户表：ch_user';


INSERT INTO `user` (`id`, `username`, `mobile`, `email`, `avatar`, `state`, `merchant_id`, `create_time`, `invite_code`)
VALUES
	(2,'auto_17768355994','17768355994',NULL,NULL,1,1,'2020-07-10 02:34:59',NULL),
	(3,'auto_17757140334','17757140334',NULL,NULL,1,1,'2020-08-15 01:12:25',NULL),
	(4,'auto_13240473740','13240473740',NULL,NULL,1,1,'2020-08-15 06:06:03',NULL),
	(5,'113240473741','13240473741',NULL,NULL,1,1,'2020-08-19 04:03:50',NULL);


DROP TABLE IF EXISTS `user_open_identity`;

CREATE TABLE `user_open_identity` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK ',
  `user_id` bigint(11) DEFAULT '0' COMMENT '用户id ',
  `identity_type` varchar(32) DEFAULT NULL COMMENT '授权类型 qq、mobile、email、weixin、weibo。。。',
  `identifier` varchar(32) DEFAULT NULL COMMENT '标识 (手机号/邮箱/用户名或第三方应用的唯一标识)',
  `credential` varchar(512) DEFAULT NULL COMMENT '登录凭证 ',
  `create_time` datetime DEFAULT NULL COMMENT '授权日期 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='第三方登录授权表：ch_user_open_identity';


INSERT INTO `user_open_identity` (`id`, `user_id`, `identity_type`, `identifier`, `credential`, `create_time`)
VALUES
	(1,2,'mobile','17768355994',NULL,'2020-07-10 02:34:59'),
	(2,3,'mobile','17757140334',NULL,'2020-08-15 01:12:25'),
	(3,4,'mobile','13240473740',NULL,'2020-08-15 06:06:03'),
	(4,5,'mobile','13240473741',NULL,'2020-08-19 04:03:50');